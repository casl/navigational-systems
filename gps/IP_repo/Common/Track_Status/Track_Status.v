//////////////////////////////////////////////////////////////////////////////////
// Company:RISE LAB IIT Madras
// Engineer:Syed M Md Zaid (ee15m039@ee.iitm.ac.in)
// 
// Create Date: 05/02/2017 12:56:01 AM
// Design Name: Track Control Circuitiry 
// Module Name: Track_Status
// Project Name: GPS
// Target Devices: VC707
// Tool Versions: 2016.2
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module Track_Status
(clk,reset,
log_complete,
Sat0_not_acquired,Sat1_not_acquired,Sat2_not_acquired,Sat3_not_acquired,
next_id,
Sat0_status,Sat1_status,Sat2_status,Sat3_status,
Sat0_id,Sat1_id,Sat2_id,Sat3_id,
count_in,
count1,count2,count3,count4,
id_to_acq);

input clk,reset;
input log_complete;// wr_en for status reg
input [31:0] count_in;
output reg [31:0] count1,count2,count3,count4;
input Sat0_not_acquired,Sat1_not_acquired,Sat2_not_acquired,Sat3_not_acquired;

input [5:0] next_id;

output Sat0_status,Sat1_status,Sat2_status,Sat3_status;
output [5:0] Sat0_id,Sat1_id,Sat2_id,Sat3_id;

//Fall edge detection
reg rg_Sat0_not_acquired,rg_Sat1_not_acquired,rg_Sat2_not_acquired,rg_Sat3_not_acquired;
always @(posedge clk)
begin
rg_Sat0_not_acquired<=Sat0_not_acquired;
rg_Sat1_not_acquired<=Sat1_not_acquired;
rg_Sat2_not_acquired<=Sat2_not_acquired;
rg_Sat3_not_acquired<=Sat3_not_acquired;
end
assign Sat0_posedge = Sat0_not_acquired && ~rg_Sat0_not_acquired;
assign Sat1_posedge = Sat1_not_acquired && ~rg_Sat1_not_acquired;
assign Sat2_posedge = Sat2_not_acquired && ~rg_Sat2_not_acquired;
assign Sat3_posedge = Sat3_not_acquired && ~rg_Sat3_not_acquired;




reg Sat0_status,Sat1_status,Sat2_status,Sat3_status;
//Logging of Data for Acquisition completes and Track status is SET ,which is later UNSET if Sat is not acquired
always @(posedge clk)
if(reset)
Sat0_status<=0;
else if(log_complete && id_to_acq==0 && ~Acq_stop)
Sat0_status<=1;
else if(Sat0_posedge)
Sat0_status<=0;

always @(posedge clk)
if(reset)
Sat1_status<=0;
else if(log_complete && id_to_acq==1 && ~Acq_stop)
Sat1_status<=1;
else if(Sat1_posedge)
Sat1_status<=0;

always @(posedge clk)
if(reset)
Sat2_status<=0;
else if(log_complete && id_to_acq==2 && ~Acq_stop)
Sat2_status<=1;
else if(Sat2_posedge)
Sat2_status<=0;

always @(posedge clk)
if(reset)
Sat3_status<=0;
else if(log_complete && id_to_acq==3 && ~Acq_stop)
Sat3_status<=1;
else if(Sat3_posedge)
Sat3_status<=0;


output [1:0] id_to_acq;
assign id_to_acq = (Sat0_status==0) ?  0:
                   (Sat1_status==0) ?  1:
                   (Sat2_status==0) ?  2:
                   (Sat3_status==0) ?  3:0;
                   
assign Acq_stop= Sat0_status && Sat1_status && Sat2_status && Sat3_status;


reg [5:0] Sat0_id,Sat1_id,Sat2_id,Sat3_id;
always @(posedge clk)
if(log_complete)
begin
case(id_to_acq)
0:
begin
Sat0_id<=next_id;
count1<=count_in;
end
1:
begin
Sat1_id<=next_id;
count2<=count_in;
end
2:
begin
Sat2_id<=next_id;
count3<=count_in;
end
3:
begin
Sat3_id<=next_id;
count4<=count_in;
end
endcase
end


endmodule
