`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 21.06.2017 14:36:33
// Module Name: comm_mem_write_ifc
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module write_com_mem(
input clk,
input reset,
input ce,
input log_complete,
input [1:0] id_to_acq,
input rd_en,  //stb from mem_read
output reg [1:0] id_track,
output wen,
output reg [17:0] wr_ptr,
output reg [17:0] samples_rem0,
output reg [17:0] samples_rem1,
output reg [17:0] samples_rem2,
output reg [17:0] samples_rem3,
output reg [17:0] start_ptr0,
output reg [17:0] start_ptr1,
output reg [17:0] start_ptr2,
output reg [17:0] start_ptr3
);
assign wen = ce;	//For writing Mem

always @(posedge clk)	//Buffer Track_mod_id
begin
	if(log_complete)
		id_track <= id_to_acq;			
end

always @(posedge clk)	//Free runnin wr_ptr
begin
	if(reset)
		wr_ptr <= 0;
	else if(wen)
		wr_ptr <= wr_ptr+1;
end

always @(posedge clk)	//Logging of start_ptr for Track Modules
begin
	if(log_complete)
	begin
	case (id_to_acq)
		0:start_ptr0 <= wr_ptr+1;
		1:start_ptr1 <= wr_ptr+1;
		2:start_ptr2 <= wr_ptr+1;
		3:start_ptr3 <= wr_ptr+1;
	endcase	
	end
end
// Samples remaining - RESET at log_complete, INC/DEC at rd_en and wen respectively
always @(posedge clk) 
begin
	if(reset || (log_complete && (id_to_acq==0)))
		samples_rem0 <= 0;
	else if(!rd_en && wen)
		samples_rem0 <= samples_rem0+1;
	else if(rd_en && !wen)
		samples_rem0 <= samples_rem0-1;
end

always @(posedge clk) 
begin
	if(reset || (log_complete && (id_to_acq==1)))
		samples_rem1 <= 0;
	else if(!rd_en && wen)
		samples_rem1 <= samples_rem1+1;
	else if(rd_en && !wen)
		samples_rem1 <= samples_rem1-1;
end

always @(posedge clk) 
begin
	if(reset || (log_complete && (id_to_acq==2)))
		samples_rem2 <= 0;
	else if(!rd_en && wen)
		samples_rem2 <= samples_rem2+1;
	else if(rd_en && !wen)
		samples_rem2 <= samples_rem2-1;
end

always @(posedge clk) 
begin
	if(reset || (log_complete && (id_to_acq==3)))
		samples_rem3 <= 0;
	else if(!rd_en && wen)
		samples_rem3 <= samples_rem3+1;
	else if(rd_en && !wen)
		samples_rem3 <= samples_rem3-1;
end

endmodule