//////////////////////////////////////////////////////////////////////////////////
// Company:RISE LAB IIT Madras
// Engineer:Syed M Md Zaid (ee15m039@ee.iitm.ac.in)
// 
// Create Date: 05/02/2017 12:56:01 AM
// Design Name: Acquire Control Circuitiry 
// Module Name: Control_SM
// Project Name: GPS
// Target Devices: VC707
// Tool Versions: 2016.2
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module Control_SM#
(parameter speedup=15,
parameter clks_per_iter=1000,
parameter samples_per_ms=4000,
parameter shifts_per_iter=500,
parameter no_freq=208)
(clk,ce,resetn,
Acq_stop,
count,state,log_complete);
input clk,ce,resetn;
input Acq_stop;

output log_complete;
output [1:0] state;
output [31:0] count;

localparam Acq_samples_read_time =  ((clks_per_iter+1)*(samples_per_ms/shifts_per_iter)*2*no_freq) ;
localparam prologue = clks_per_iter;
localparam Acq_no_of_cycles = Acq_samples_read_time + prologue;
localparam process_time = (Acq_no_of_cycles/(speedup));
localparam log_count = 2*samples_per_ms;
localparam process_count=process_time-log_count;

reg [31:0] count;
    always @(posedge clk)
    if(state_change || (~resetn))
    count<=0;
    else if(~count_pause && ce)
    count<=count+1;

reg [1:0] state;    
    assign count_pause = (state==2) & (count==process_count-1) && Acq_stop;
                                       
    always @(posedge clk) //State Machine
    begin
    if(~resetn)
        state<=0;
    else 
    begin    
        case(state)
        0: // IDLE state
            begin
            if(state_change)
            state<=1;
            end
        
        1: // Log state
            begin
            if(state_change)
            state<=2;
            end
        
        2: // Process State
            begin
            if(state_change)
            state<=1;
            end
        default :
            state<=0;    
        
        endcase
    end
    end 
    
    assign state_change = (state==0) ? (count[9] && ce) :
                          (state==1) ? ((count==log_count-1) && ce):
                                       ((count==process_count-1) && ~Acq_stop && ce);
    
    assign log_complete = (state==1) && (count==log_count-1) && ce;

endmodule
