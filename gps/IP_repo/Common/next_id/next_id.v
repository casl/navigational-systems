`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/10/2017 05:48:37 AM
// Design Name: 
// Module Name: next_id
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module next_id(clk,reset,
log_complete,
id_in1,id_in2,id_in3,id_in4,
Valid1,Valid2,Valid3,Valid4,
next_id);

input clk,reset;
input log_complete;
input [5:0] id_in1,id_in2,id_in3,id_in4;
input Valid1,Valid2,Valid3,Valid4;
output [5:0] next_id;

reg [4:0] id;
wire [4:0] id1,id2,id3,id4,id5,new_id;

assign id1=id+1;
assign id2=id+2;
assign id3=id+3;
assign id4=id+4;
assign id5=id+5;

assign match_id1 = ((id1==(id_in1-1)) && Valid1) ||
                   ((id1==(id_in2-1)) && Valid2) ||
                   ((id1==(id_in3-1)) && Valid3) ||
                   ((id1==(id_in4-1)) && Valid4);
assign match_id2 = ((id2==(id_in1-1)) && Valid1) ||
                   ((id2==(id_in2-1)) && Valid2) ||
                   ((id2==(id_in3-1)) && Valid3) ||
                   ((id2==(id_in4-1)) && Valid4);
assign match_id3 = ((id3==(id_in1-1)) && Valid1) ||
                   ((id3==(id_in2-1)) && Valid2) ||
                   ((id3==(id_in3-1)) && Valid3) ||
                   ((id3==(id_in4-1)) && Valid4);
assign match_id4 = ((id4==(id_in1-1)) && Valid1) ||
                   ((id4==(id_in2-1)) && Valid2) ||
                   ((id4==(id_in3-1)) && Valid3) ||
                   ((id4==(id_in4-1)) && Valid4);

assign new_id = (match_id1==0) ? id1 :
                (match_id2==0) ? id2 :
                (match_id3==0) ? id3 :
                (match_id4==0) ? id4 : id5;

always @(posedge clk)
if(reset)
id<=0;
else if (log_complete)
id<=new_id;

assign next_id = id + 1;                
endmodule
