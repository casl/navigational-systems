`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 03/05/2018 07:16:55 PM
// Module Name: correlator_bank
// Project Name: correlator_bank_xor
// Description: correlate 4 samples with 4 prn bits in each cycle for 1000 cycles,repeat for next block and iterations
//////////////////////////////////////////////////////////////////////////////////
//Inputs:
//1.strbe : Keep Adding,reset on negedge
//2.iterno : For maxrst_in
//3. Cosout,Sinout,Prn : inputs
//4. sums from adder give all the possible combinations for given prn


module correlator_bank#
(parameter in_width=18,
parameter inter_crltn_sum_width1=in_width+1,
parameter inter_crltn_sum_width2=inter_crltn_sum_width1+1, 
parameter samples_per_ms_log2=12,
parameter crltn_sum_width=30, //in_width+samples_per_ms_log2;
parameter no_iter_per_freq_log2=3,
parameter adds_per_clk=4,
parameter adds_per_clk_log2=2,
parameter shifts_per_iter=500,
parameter shifts_per_iter_log2=9,
parameter clks_per_iter=1000,
parameter clks_per_iter_log2=10)
(clk,reset,
sumI_0000,sumI_0001,sumI_0011,sumI_0111,
sumQ_0000,sumQ_0001,sumQ_0011,sumQ_0111,	
strbe,iterno,prn,
x_in,y_in,pointer_mag,
strbe_vect,maxrst_in,
sumI_out,
sumQ_out
);	

input clk,reset;
input [shifts_per_iter+adds_per_clk-1:0] prn;           // All code bits for 500 Shifters
input strbe;                                            // When strbe is high Accumulation happens
input [no_iter_per_freq_log2-1:0] iterno;               // For knowing when a chunk completes [(iter_no==0) && pointer_mag == shifts_per_iter-1 ] All sums are spitted out
input [inter_crltn_sum_width2-1:0] sumI_0000,sumI_0001,sumI_0011,sumI_0111;
input [inter_crltn_sum_width2-1:0] sumQ_0000,sumQ_0001,sumQ_0011,sumQ_0111;


output reg strbe_vect;                                  //Strobe for result , This is used by Cordic Magnitude Block
output wire [crltn_sum_width-1:0] x_in,y_in;            //Values of sum coming serially
output reg maxrst_in;                                   //For sorter to reset its MAX register
output reg [shifts_per_iter_log2-1:0] pointer_mag;        //For debug

output [crltn_sum_width-1:0] sumI_out;
output [crltn_sum_width-1:0] sumQ_out;

assign sumI_out=sumI[0];
assign sumQ_out=sumQ[0];

//negedge on strbe indicates completion of an iter ,Results are to be copied onto output regisers on this instant        
reg rg_strbe;
always @(posedge clk)
rg_strbe<=strbe;
assign strbenegedge=(~strbe) && (rg_strbe);

reg [crltn_sum_width-1:0] sumQ [shifts_per_iter-1:0]; // accumulators
reg [crltn_sum_width-1:0] sumI [shifts_per_iter-1:0]; // accumulators

genvar i;
generate
	for (i=0; i < (shifts_per_iter); i=i+1)
	begin: Adders
		wire [2:0] encoded_prn;
		wire [3:0] prn_bits,prn_xor;

		assign prn_bits[0] = prn[i+3];
		assign prn_bits[1] = prn[i+2];
		assign prn_bits[2] = prn[i+1];
		assign prn_bits[3] = prn[i];

		assign prn_xor[0] = prn_bits[0] ^ prn_bits[3];
		assign prn_xor[1] = prn_bits[1] ^ prn_bits[3];
		assign prn_xor[2] = prn_bits[2] ^ prn_bits[3];
		assign prn_xor[3] = prn_bits[3];

		assign encoded_prn = prn_xor[2:0] == 0 ? 2'b00 :
							 prn_xor[2:0] == 1 ? 2'b01 :
							 prn_xor[2:0] == 3 ? 2'b10 :
							 prn_xor[2:0] == 7 ? 2'b11 : 2'b00;

		//MUX 4:1
		wire [inter_crltn_sum_width2-1:0] Imux1_out, Imux2_out, Imux3_out, neg_sel_outI, neg_sel_outQ ;
		wire [inter_crltn_sum_width2-1:0] Qmux1_out, Qmux2_out, Qmux3_out ;
		wire [crltn_sum_width-1:0] sel_outI, sel_outQ ;

		assign Imux1_out = encoded_prn[0]==0 ? sumI_0000 : sumI_0001;
		assign Imux2_out = encoded_prn[0]==0 ? sumI_0011 : sumI_0111;

		assign Imux3_out = encoded_prn[1]==0 ? Imux1_out : Imux2_out;

		assign neg_sel_outI = -Imux3_out;

		assign sel_outI = prn_xor[3] == 0 ? {{(crltn_sum_width-inter_crltn_sum_width2){Imux3_out[inter_crltn_sum_width2-1]}},Imux3_out} :
											{{(crltn_sum_width-inter_crltn_sum_width2){neg_sel_outI[inter_crltn_sum_width2-1]}},neg_sel_outI} ;

		assign Qmux1_out = encoded_prn[0]==0 ? sumQ_0000 : sumQ_0001;
		assign Qmux2_out = encoded_prn[0]==0 ? sumQ_0011 : sumQ_0111;

		assign Qmux3_out = encoded_prn[1]==0 ? Qmux1_out : Qmux2_out;

		assign neg_sel_outQ = -Qmux3_out;

		assign sel_outQ = prn_xor[3] == 0 ? {{(crltn_sum_width-inter_crltn_sum_width2){Qmux3_out[inter_crltn_sum_width2-1]}},Qmux3_out} :
											{{(crltn_sum_width-inter_crltn_sum_width2){neg_sel_outQ[inter_crltn_sum_width2-1]}},neg_sel_outQ} ;

// check with number of 0 in prn added to sum
// Conditional Add/Sub of samples
		always @(posedge clk)
			begin
				if(strbe) begin
					sumI[i]<=sumI[i]+sel_outI;
					sumQ[i]<=sumQ[i]+sel_outQ;						
				end
				if (reset || strbenegedge) begin
					sumI[i]<=0;
					sumQ[i]<=0;												
				end
				else 
				begin
				end
			end
	end
endgenerate

//*****************************************************//
//.Copies the Accumulator results into other Registers //
//.For Magnitude calculation and MAXIMA finding        //
//*****************************************************//
reg [crltn_sum_width-1:0] ResultI [shifts_per_iter-1:0];
reg [crltn_sum_width-1:0] ResultQ [shifts_per_iter-1:0];
reg [shifts_per_iter_log2:0] ii;
always @(posedge clk)
begin
   if(strbenegedge)
begin
	for	(ii=0;ii<shifts_per_iter;ii=ii+1)
	begin
		ResultI[ii]=sumI[ii];
		ResultQ[ii]=sumQ[ii];
	end
end
end

assign x_in=ResultI[pointer_mag];
assign y_in=ResultQ[pointer_mag];


//*******************************************************//
//.Turns ON strobe for CORDIC_VECT on the negedge on     //
//.strobe_out of CORDIC_ROTOR(completion of an iteration)//
//*******************************************************//
always @(posedge clk)
if(reset || pointer_mag==shifts_per_iter-1)
	strbe_vect<=0;
else if (strbenegedge)
	strbe_vect<=1;

//Maxrst is given out for sorter block After a chunk is completes//
always @(posedge clk)
if(reset)
maxrst_in<=1;
else 
if((iterno==0) && (pointer_mag==shifts_per_iter-1))
maxrst_in<=1;
else if(strbenegedge)
maxrst_in<=0;	

//.Steers inputs one by one for CORDIC_VECT //
always @(posedge clk)
if(reset || pointer_mag==shifts_per_iter-1)
	pointer_mag<=0;
else if(strbe_vect)
	pointer_mag<=pointer_mag+1;


endmodule
