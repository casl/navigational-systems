module count#
(parameter clks_per_iter=1000,
 parameter no_iter_per_freq=8,
 parameter clks_per_iter_log2=10,//$clog2(clks_per_iter)
 parameter no_iter_per_freq_log2 = 3//$clog2(no_iter_per_freq;
 )
(clk,reset,count,pointer,iterno,chunk_no,stb_in,iter_in,chunk_in,
iter_change,chunk_change,freq_ptr_change);


input clk,reset;
output [clks_per_iter_log2-1:0] count;
output [clks_per_iter_log2:0] pointer;
output reg [no_iter_per_freq_log2-1:0] iterno;
output reg chunk_no;
output iter_change,chunk_change,freq_ptr_change;
output reg stb_in,iter_in,chunk_in;

//*********************************************//
//.Counter: Tracks offset of current iteration //
//*********************************************//
	reg [clks_per_iter_log2-1:0] count;
	always @(posedge clk)
		if(iter_change || reset )
			count<=0;
		else
			count<=count+1;

//*********************************************//
//.Counter: Tracks iteration number            //
//*********************************************//
	
	always @(posedge clk)
		if(chunk_change || reset)
			iterno<=0;
		else if(iter_change)
			iterno<=iterno+1;
    
//************************************************//
//.Two consecutive 1ms Data chunks processed ..   //
// for a given frequency to avoid Nav bit boundary//
//************************************************//
	always @(posedge clk)
		if(reset)
			chunk_no<=0;
		else if(chunk_change)
			chunk_no<=~(chunk_no);

	always @(posedge clk)
	begin
		if (reset || iter_change)
			stb_in<=0;
		else begin
			stb_in<=1;
		end
	end

	always @(posedge clk)
	begin
	chunk_in<=chunk_change;
	iter_in<=iter_change;
	end

assign iter_change = (count==clks_per_iter);
assign chunk_change = (iterno==(no_iter_per_freq-1)) && iter_change;
assign freq_ptr_change= (iterno==(no_iter_per_freq-1)) && (count==clks_per_iter-2) && (chunk_no==1);
assign pointer=(chunk_no==0) ? count : count+clks_per_iter;			
endmodule