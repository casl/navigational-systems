`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/21/2017 07:53:53 AM
// Design Name: 
// Module Name: present_results
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module present_results#
(
parameter no_freq_log2=8,
parameter samples_per_ms_log2=12,
parameter samples_per_ms=4000,
parameter crltn_sum_width=32
)
(
input clk,reset,
input maxrst_out,
input chunk_no,
input [no_freq_log2-1:0] freq_pointer,
input [crltn_sum_width-1:0] max1,
input [crltn_sum_width-1:0] max2,
input [samples_per_ms_log2-1:0] maxaddr,


output reg SAT_complete,                           
output reg SAT_Acquired,                           
output reg [crltn_sum_width-1:0] SAT_Maxima,
output reg [crltn_sum_width-1:0] SAT_Maxima_2,       
output reg [samples_per_ms_log2-1:0] SAT_CodePhase,
output reg [no_freq_log2-1:0] SAT_Freq
);

reg rg_maxrst_out;

//.Delayed Posedge for signalling completion of processing of a PRN code //
always @(posedge clk)
begin
rg_maxrst_out<=maxrst_out;
end

assign chunk_complete = maxrst_out && ~rg_maxrst_out;

reg rg_chunk_complete,rg_rg_chunk_complete;
always @(posedge clk)
begin
rg_chunk_complete<=chunk_complete;
rg_rg_chunk_complete<=rg_chunk_complete;
end

reg [samples_per_ms_log2-1:0] CodePhase;
reg [crltn_sum_width-1:0] rg_max1;
reg [crltn_sum_width-1:0] rg_max2;
reg Hit;
//***********************************************//
//After processing of chunk completes,this block //
//updates Maxima of correlation result and also  //
//registers Codephase and frequency at which max //
//occured,At completion of PRN code this block   //
//updates global registers containing data of    //
//Acquired Satellites                            //
//***********************************************// 
always @(posedge clk)
begin	
	if(chunk_complete)
	begin
		rg_max1<=max1;
		rg_max2<=max2;
		CodePhase<=maxaddr;
		//Hit<=(max1>(max2+ (max2>>1) - (max2>>3) + (max2>>4)));//Condition for 'HIT'is Max1 > 1.414 times Max2.1.414 is implemented as (1+(1/2)-(1/8)+(1/16))
		Hit<=(max1>(max2+ (max2>>1)));
	end
end

assign PRN_complete=rg_rg_chunk_complete & freq_pointer==0 & chunk_no==0 ;





always @(posedge clk)
begin
SAT_complete<=PRN_complete;
SAT_Acquired<=PRN_Acquired;
SAT_Maxima<=PRN_Maxima;
SAT_Maxima_2<=PRN_Maxima_2;
SAT_CodePhase<=PRN_CodePhase;
end

reg PRN_Acquired;                           
reg [crltn_sum_width-1:0] PRN_Maxima;    
reg [crltn_sum_width-1:0] PRN_Maxima_2;       
reg [samples_per_ms_log2-1:0] PRN_CodePhase;
always @(posedge clk)
begin
    if(rg_chunk_complete)
    begin
        if(rg_max1>PRN_Maxima)
        begin
        PRN_Maxima<=rg_max1;
        PRN_Maxima_2<=rg_max2;
        PRN_Acquired<=Hit;
        PRN_CodePhase<=CodePhase;
        SAT_Freq<=(chunk_no==0)? (freq_pointer-1) : (freq_pointer);
        end
    end
    
    if(PRN_complete)
    begin
        PRN_Maxima<=0;
        PRN_Acquired<=0;
    end
end    


endmodule