module adder#
(
parameter in_width=18,
parameter inter_crltn_sum_width=in_width+1,
parameter output_width=inter_crltn_sum_width+1,
parameter adds_per_clk=4
)
(COSout,SINout
,sumI_0000,sumI_0001,sumI_0011,sumI_0111
,sumQ_0000,sumQ_0001,sumQ_0011,sumQ_0111);

input [((adds_per_clk*in_width)-1):0] COSout,SINout;    // 4 inputs after Doppler rotation

output signed [output_width-1:0] sumI_0000; 
output signed [output_width-1:0] sumI_0001;
output signed [output_width-1:0] sumI_0011;
output signed [output_width-1:0] sumI_0111;
output signed [output_width-1:0] sumQ_0000; 
output signed [output_width-1:0] sumQ_0001;
output signed [output_width-1:0] sumQ_0011;
output signed [output_width-1:0] sumQ_0111;

//Sign extension Block
wire [inter_crltn_sum_width-1:0] II [0:adds_per_clk-1];
wire [inter_crltn_sum_width-1:0] QQ [0:adds_per_clk-1];
genvar ab;
generate
	for (ab=1; ab <= adds_per_clk;ab=ab+1)
	begin: sign_extend
	wire [in_width-1:0] I,Q;
        assign I=COSout[((ab*in_width)-1)-:in_width];
        assign Q=SINout[((ab*in_width)-1)-:in_width];
        assign II[ab-1] = { {(inter_crltn_sum_width-in_width){I[in_width-1]}} ,I};
        assign QQ[ab-1] = { {(inter_crltn_sum_width-in_width){Q[in_width-1]}} ,Q};
	end
endgenerate

// intermediate accumulators 19 bits
wire [inter_crltn_sum_width-1:0] lvl1_sumI_1; 
wire [inter_crltn_sum_width-1:0] lvl1_sumQ_1;
wire [inter_crltn_sum_width-1:0] lvl1_sumI_2;
wire [inter_crltn_sum_width-1:0] lvl1_sumQ_2;
wire [inter_crltn_sum_width-1:0] lvl1_sumI_3; 
wire [inter_crltn_sum_width-1:0] lvl1_sumQ_3;
wire [inter_crltn_sum_width-1:0] lvl1_sumI_4;
wire [inter_crltn_sum_width-1:0] lvl1_sumQ_4;

//sign extended intermediate accumulators 20 bits
wire [output_width-1:0] se_lvl1_sumI_1;
wire [output_width-1:0] se_lvl1_sumQ_1;
wire [output_width-1:0] se_lvl1_sumI_2;
wire [output_width-1:0] se_lvl1_sumQ_2;
wire [output_width-1:0] se_lvl1_sumI_3;
wire [output_width-1:0] se_lvl1_sumQ_3;
wire [output_width-1:0] se_lvl1_sumI_4;
wire [output_width-1:0] se_lvl1_sumQ_4;


//two level 1 adders
assign lvl1_sumI_1 = -II[adds_per_clk-4] - II[adds_per_clk-3] ; // -a-b				  
assign lvl1_sumI_2 =  II[adds_per_clk-2] + II[adds_per_clk-1] ; //  c+d
assign lvl1_sumI_3 = -II[adds_per_clk-4] + II[adds_per_clk-3] ; // -a+b				     
assign lvl1_sumI_4 = -II[adds_per_clk-2] + II[adds_per_clk-1] ; // -c+d

assign lvl1_sumQ_1 = -QQ[adds_per_clk-4] - QQ[adds_per_clk-3] ; // -a-b				  
assign lvl1_sumQ_2 =  QQ[adds_per_clk-2] + QQ[adds_per_clk-1] ; //  c+d
assign lvl1_sumQ_3 = -QQ[adds_per_clk-4] + QQ[adds_per_clk-3] ; // -a+b				     
assign lvl1_sumQ_4 = -QQ[adds_per_clk-2] + QQ[adds_per_clk-1] ; // -c+d				     

//sign extension 20 bits
assign se_lvl1_sumI_1 = { {(output_width-inter_crltn_sum_width){lvl1_sumI_1[inter_crltn_sum_width-1]}}, lvl1_sumI_1 }; // -a-b
assign se_lvl1_sumI_2 = { {(output_width-inter_crltn_sum_width){lvl1_sumI_2[inter_crltn_sum_width-1]}}, lvl1_sumI_2 }; //  c+d
assign se_lvl1_sumI_3 = { {(output_width-inter_crltn_sum_width){lvl1_sumI_3[inter_crltn_sum_width-1]}}, lvl1_sumI_3 }; // -a+b
assign se_lvl1_sumI_4 = { {(output_width-inter_crltn_sum_width){lvl1_sumI_4[inter_crltn_sum_width-1]}}, lvl1_sumI_4 }; // -c+d

assign se_lvl1_sumQ_1 = { {(output_width-inter_crltn_sum_width){lvl1_sumQ_1[inter_crltn_sum_width-1]}}, lvl1_sumQ_1 }; // -a-b
assign se_lvl1_sumQ_2 = { {(output_width-inter_crltn_sum_width){lvl1_sumQ_2[inter_crltn_sum_width-1]}}, lvl1_sumQ_2 }; //  c+d
assign se_lvl1_sumQ_3 = { {(output_width-inter_crltn_sum_width){lvl1_sumQ_3[inter_crltn_sum_width-1]}}, lvl1_sumQ_3 }; // -a+b
assign se_lvl1_sumQ_4 = { {(output_width-inter_crltn_sum_width){lvl1_sumQ_4[inter_crltn_sum_width-1]}}, lvl1_sumQ_4 }; // -c+d

//four adders 20 bits
assign sumI_0000 = se_lvl1_sumI_1 - se_lvl1_sumI_2 ; //-a-b-(c+d)
assign sumI_0001 = se_lvl1_sumI_1 + se_lvl1_sumI_4; //-a-b-c+d
assign sumI_0011 = se_lvl1_sumI_1 + se_lvl1_sumI_2; //-a-b+c+d
assign sumI_0111 = se_lvl1_sumI_3 + se_lvl1_sumI_2; //-a+b+c+d

assign sumQ_0000 = se_lvl1_sumQ_1 - se_lvl1_sumQ_2 ; //-a-b-(c+d)
assign sumQ_0001 = se_lvl1_sumQ_1 + se_lvl1_sumQ_4 ; //-a-b-c+d
assign sumQ_0011 = se_lvl1_sumQ_1 + se_lvl1_sumQ_2 ; //-a-b+c+d
assign sumQ_0111 = se_lvl1_sumQ_3 + se_lvl1_sumQ_2 ; //-a+b+c+d

endmodule
