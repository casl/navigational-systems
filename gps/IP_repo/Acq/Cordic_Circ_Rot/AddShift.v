module AddShift#
(parameter intrmdte_wdth=22,
parameter iters_per_stage=4,
parameter j=1)
(xin,yin,zin,
xout,yout,zout);

input signed [intrmdte_wdth-1:0] xin,yin;
input signed [31:0] zin;

output [intrmdte_wdth-1:0] xout,yout;
output [31:0] zout; 
// Generate table of atan values
wire signed [31:0] atan_table [0:30];
assign atan_table[00] = 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
assign atan_table[01] = 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
assign atan_table[02] = 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
assign atan_table[03] = 'b00000101000100010001000111010100; // atan(2^-3)
assign atan_table[04] = 'b00000010100010110000110101000011;
assign atan_table[05] = 'b00000001010001011101011111100001;
assign atan_table[06] = 'b00000000101000101111011000011110;
assign atan_table[07] = 'b00000000010100010111110001010101;
assign atan_table[08] = 'b00000000001010001011111001010011;
assign atan_table[09] = 'b00000000000101000101111100101110;
assign atan_table[10] = 'b00000000000010100010111110011000;
assign atan_table[11] = 'b00000000000001010001011111001100;
assign atan_table[12] = 'b00000000000000101000101111100110;
assign atan_table[13] = 'b00000000000000010100010111110011;
assign atan_table[14] = 'b00000000000000001010001011111001;
assign atan_table[15] = 'b00000000000000000101000101111100;
assign atan_table[16] = 'b00000000000000000010100010111110;
assign atan_table[17] = 'b00000000000000000001010001011111;
assign atan_table[18] = 'b00000000000000000000101000101111;
assign atan_table[19] = 'b00000000000000000000010100010111;
assign atan_table[20] = 'b00000000000000000000001010001011;
assign atan_table[21] = 'b00000000000000000000000101000101;
assign atan_table[22] = 'b00000000000000000000000010100010;
assign atan_table[23] = 'b00000000000000000000000001010001;
assign atan_table[24] = 'b00000000000000000000000000101000;
assign atan_table[25] = 'b00000000000000000000000000010100;
assign atan_table[26] = 'b00000000000000000000000000001010;
assign atan_table[27] = 'b00000000000000000000000000000101;
assign atan_table[28] = 'b00000000000000000000000000000010;
assign atan_table[29] = 'b00000000000000000000000000000001;
assign atan_table[30] = 'b00000000000000000000000000000000;


wire signed [intrmdte_wdth-1:0] xi [iters_per_stage:0];
wire signed [intrmdte_wdth-1:0] yi [iters_per_stage:0];
wire [31:0] zi [iters_per_stage:0];
assign xi[0]=xin;
assign yi[0]=yin;
assign zi[0]=zin;


genvar lv;
generate
	for (lv=0;lv<iters_per_stage;lv=lv+1)
    begin: ShiftAdd 
    wire signed [intrmdte_wdth-1:0] x_shr,y_shr;
    wire [31:0] angle;
        assign x_shr = xi[lv] >>> (j+lv);
        assign y_shr = yi[lv] >>> (j+lv);
        assign angle = zi[lv];
        assign z_sign= angle[31];        

        assign xi[lv+1] = z_sign ? xi[lv] + y_shr : xi[lv] - y_shr;                      
        assign yi[lv+1] = z_sign ? yi[lv] - x_shr : yi[lv] + x_shr;                      
        assign zi[lv+1] = z_sign ? zi[lv] + atan_table[j+lv] : zi[lv] - atan_table[j+lv];   
    end     
endgenerate       


assign xout=xi[iters_per_stage];
assign yout=yi[iters_per_stage];
assign zout=zi[iters_per_stage];
endmodule
