`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 03/05/2018 03:58:58 AM
// Module Name: prncode_gen
// Description: generates GA,GB for the EPILOGUE in prn_code_chain 
//////////////////////////////////////////////////////////////////////////////////
//Inputs :
//strbe: Code keeps Ticking
//Change_offset : loads new values into registers G1,G2
//Code-reset : Loads init values 
//cp_match-logs value for next iteration

module prncode_gen#
(parameter code_phase_per_sample=1098437885, //2^32*(1023/samples_per_ms)  
parameter [31:0] prn_code_phase_per_sample=660350741, //remainder of (shifts_per_iter+adds_per_clk+1)*code_phase_per_sample , 2^32
parameter init_G1=10'h263, //STATE of shifts_per_iter+adds_per_clk+1
parameter init_G2=10'h37A
)
(clk,reset,
strbe,code_reset,change_offset,cp_match,
GA1,GA2,GA3,GA4,
GB1,GB2,GB3,GB4);  

input clk,reset; 
input strbe,cp_match,code_reset,change_offset; 

//***********************************//
// Keeps track of code phase         //
//increments code phase,             //
//loads new phase for new shifts and //
//resets when chunk completes        //
//***********************************//
  reg [31:0] cmltve_phse_frac1;
  always @(posedge clk)
  if(code_reset || reset)
       cmltve_phse_frac1<=prn_code_phase_per_sample;
   else if(change_offset)
       cmltve_phse_frac1<=phase_nxt;
   else if(strbe)
  	cmltve_phse_frac1<=cmltve_phse_frac4+code_phase_per_sample; 


//Calculates whether or not to shift for future 4 instants
wire [31:0] neg_wire1,neg_wire2,neg_wire3,neg_wire4;
wire [31:0] cmltve_phse_frac2,cmltve_phse_frac3,cmltve_phse_frac4;        
   assign cmltve_phse_frac2 = cmltve_phse_frac1 + code_phase_per_sample;
   assign cmltve_phse_frac3 = cmltve_phse_frac2 + code_phase_per_sample;
   assign cmltve_phse_frac4 = cmltve_phse_frac3 + code_phase_per_sample;
   
   assign neg_wire1 = ~cmltve_phse_frac1;
   assign neg_wire2 = ~cmltve_phse_frac2;
   assign neg_wire3 = ~cmltve_phse_frac3;
   assign neg_wire4 = ~cmltve_phse_frac4;
   
   wire [3:0] shift;
   assign shift[3] = ((neg_wire1)<=(code_phase_per_sample-1));
   assign shift[2] = ((neg_wire2)<=(code_phase_per_sample-1));
   assign shift[1] = ((neg_wire3)<=(code_phase_per_sample-1));
   assign shift[0] = ((neg_wire4)<=(code_phase_per_sample-1));
   

//Calculate future 4 STATES corresponding to new 4 shifts   
output reg [9:0] GA1,GB1;
output wire [9:0] GA2,GA3,GA4;
wire [9:0] GAnext,GBnext;
output wire [9:0] GB2,GB3,GB4;
   assign GA2=shift[3] ? {GA1[7]^GA1[0],GA1[9:1]} : GA1;
   assign GB2=shift[3] ? {GB1[8]^GB1[7]^GB1[4]^GB1[2]^GB1[1]^GB1[0],GB1[9:1]}  :  GB1;
   
   assign GA3=shift[2] ? {GA2[7]^GA2[0],GA2[9:1]} : GA2;
   assign GB3=shift[2] ? {GB2[8]^GB2[7]^GB2[4]^GB2[2]^GB2[1]^GB2[0],GB2[9:1]}  :  GB2;

   assign GA4=shift[1] ? {GA3[7]^GA3[0],GA3[9:1]} : GA3;
   assign GB4=shift[1] ? {GB3[8]^GB3[7]^GB3[4]^GB3[2]^GB3[1]^GB3[0],GB3[9:1]}  :  GB3;

   assign GAnext=shift[0] ? {GA4[7]^GA4[0],GA4[9:1]} : GA4;
   assign GBnext=shift[0] ? {GB4[8]^GB4[7]^GB4[4]^GB4[2]^GB4[1]^GB4[0],GB4[9:1]}  : GB4;

//Jumps 4 states every clock cycle for which strobe is ON
   always@(posedge clk)
   begin
       if(reset || code_reset) begin //At chunk change      
           GA1<=init_G1;    
           GB1<=init_G2;             
       end else if(change_offset) begin //For new iteration
           GA1<=rg_G1_nextiter;
           GB1<=rg_G2_nextiter;                                
       end else if(strbe) begin
           GA1<=GAnext;
           GB1<=GBnext;
       end 
   end

//Captures Next state for next iteration   
   reg [9:0] rg_G1_nextiter,rg_G2_nextiter;
   reg [31:0] phase_nxt;
   always @(posedge clk)
   if(cp_match)
   begin
       rg_G1_nextiter<= GA1;       
       rg_G2_nextiter<= GB1;       
       phase_nxt<=cmltve_phse_frac1; // Depends on offset
   end        

endmodule
