`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/09/2017 08:37:50 PM
// Design Name: 
// Module Name: fifo_mem_ifc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module in_fifo_ifc
(clk,reset,
IN_FIFO_EMPTY,IN_FIFO_RD_EN);
input clk,reset;
input IN_FIFO_EMPTY;

output IN_FIFO_RD_EN;
 
reg state; //0-WAIT 1-DEQUEUE

//SM - STAY IN WAIT STATE TILL FIFO IS NOT EMPTY
//And then dequeue and get back to WAIT STATE
always @(posedge clk)
if(reset)
state<=0;
else
case(state)
0:
if(~IN_FIFO_EMPTY)
state<=1;
1:
state<=0;
endcase

assign IN_FIFO_RD_EN= state==1;

endmodule
