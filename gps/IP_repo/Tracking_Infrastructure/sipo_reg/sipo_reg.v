`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 23.03.2018 05:18:21
// Module Name: sipo_reg
// Description: 30-bit serial-in parallel-out register to store 30-nav bits which form a word.
//////////////////////////////////////////////////////////////////////////////////


module sipo_reg(
clk, reset, shift,
din, dout, 
rd_en,count  //debug
);

input clk, reset, shift;
input din;
output reg [29:0] dout;

integer i;
always @(posedge clk)
    if(reset || rd_en)
            dout <= 0;
    else if(shift) begin
        dout[29] <= din;
        for(i=29;i>0;i=i-1)
            dout[i-1] <= dout[i];    
    end   

//dout to be read when counter becomes 30
output reg [5:0] count;

always @(posedge clk)
    if(reset || rd_en)
        count <= 0;
    else if(shift)
        count <= count + 1;
        
output wire rd_en;
assign rd_en = count == 30;        

endmodule        
