`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/02/2017 04:59:44 PM
// Design Name: 
// Module Name: Valid_FIFO_rd_ifc_logic
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Buffer_Acq_Results
(clk,reset,
Sat_Status,Sat_not_acquired,
SAT_complete,SAT_Acquired,SAT_Codephase,SAT_phi,SAT_Max1,SAT_Max2,
Track_Failed,
Acq_posedge,cp,phi,max1,max2,
Acq_Failed,state);

input clk,reset;
input Sat_Status;

//Data from Acquisition IP block
input SAT_complete,SAT_Acquired;
input [11:0] SAT_Codephase;
input [31:0] SAT_phi;
//Feedback from Core 
input Track_Failed;
input [31:0] SAT_Max1,SAT_Max2;
//Data to Track
output [31:0] phi;
output [11:0] cp;
output [31:0] max1,max2;
//Start signal to Track
output Acq_posedge,Acq_Failed;
//Failure of acquiring or tracking satellite
output Sat_not_acquired;


output reg [2:0] state;

reg [11:0] cp;reg [31:0] phi; 
reg [31:0] max1,max2;
always @(posedge clk)
if(reset)
state<=0;
else
begin
case(state)
0:  //Wait for our turn
if(Sat_Status)
state<=1;
1:  //Wait for Acquisition to happen
begin

if(SAT_complete)    //Acquisition complete ?
begin

cp<=SAT_Codephase;
phi<=SAT_phi;
max1<= SAT_Max1;
max2<= SAT_Max2;
if(SAT_Acquired)    //Acquired?
state<=2;
else
state<=0;
end

end
2:
if(rg_rg_Track_Failed)
state<=0;
endcase
end

reg [2:0] prev_state;
always @(posedge clk)
prev_state<=state;

assign Acq_posedge = prev_state==1 && state==2;
assign Acq_Failed = ((state==1) && SAT_complete && ~SAT_Acquired); //Acq_Failed

assign Sat_not_acquired = Acq_Failed || ((state==2) && Track_Failed);

reg rg_Track_Failed,rg_rg_Track_Failed;
always @(posedge clk)
begin
rg_Track_Failed<=Track_Failed;
rg_rg_Track_Failed<=rg_Track_Failed;
end

endmodule
