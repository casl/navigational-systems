`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 21.06.2017 15:23:40
// Module Name: FIFO_read_ifc
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module FIFO_read#
(parameter id = 0 )
(
input clk, 
input reset, 
input ce,
input stop,
input [1:0] id_read,
input almost_empty,
input [11:0] fifo_data_count,
input RDY,
input BUSY,
input Sat_Status,
output FIFO_rd_en,
output sel,
output start_rd_from_FIFO,
output reg [1:0] state
);


always @(posedge clk)
begin
	if(reset)
		state<=0;
	else begin
		case(state)
		0:
		if(start_rd_from_FIFO) 
		begin
			state <= 1;		
		end	
		1:
		if(~Sat_Status)
		    state <= 0;
		else if(BUSY)
			state <= 3;
		else if(almost_empty)
			state <= 2;
		2:
		if(~Sat_Status)
            state <= 0;
        else if(fifo_data_count>=32) 
			state <= 1;
		3:
		if(~Sat_Status)
            state <= 0;
        else if(RDY)
			state <= 1;
		endcase
	end
end 
assign FIFO_rd_en = state == 1;
assign start_rd_from_FIFO = id_read==id && stop && Sat_Status;
assign sel =state ==0; 

endmodule