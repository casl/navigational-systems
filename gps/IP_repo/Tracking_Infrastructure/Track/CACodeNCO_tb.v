module CACodeNCO_tb;
// localparam phase_per_sample_width = 32;
// localparam code_length_log2 = 10;
// localparam code_length =1023;

reg clk=0, reset;
wire strobe;
wire [5:0] PRN;
wire [32-1:0] phase_per_sample;
wire O_EC, O_PC,O_LC;
wire block_change;
wire [32-1:0] cmltve_phse_frac;
wire [10-1:0] cmltve_phse_int;

always #10 clk=!clk;
integer f;
initial begin
	$dumpfile("cacode.vcd");
	$dumpvars(0,CACodeNCO_tb);
  	f = $fopen("/home/yashodhara/MTP/GPS/IP_repo/Tracking_Infrastructure/Track/src/early.txt","w");
end

integer run_cycles=4010*20; //assume state high for 500 cycles

initial begin
# 0 reset=1;
# 33 reset=0;
# run_cycles 
	$finish;
end

always @(posedge clk) begin
	if(~reset)
    	$fwrite (f,"%b,",O_EC);
end

reg [11:0] count;
always @(posedge clk) begin
	if (reset || count==4000)
		count <= 0;
	else
		count <= count + 1;
end

assign strobe = count != 0;
assign PRN = 16;
assign phase_per_sample = 1098437885; 

cacodeNCO#
(.phase_per_sample_width(32),
 .code_length_log2(10),
 .code_length(1023))
code1
(.clk(clk),
.reset(reset),
.strobe(strobe),
.PRN(PRN),
.phase_per_sample(phase_per_sample),
.O_EC(O_EC),
.O_PC(O_PC),
.O_LC(O_LC),
.block_change(block_change),
.cmltve_phse_frac(cmltve_phse_frac),
.cmltve_phse_int(cmltve_phse_int));

endmodule