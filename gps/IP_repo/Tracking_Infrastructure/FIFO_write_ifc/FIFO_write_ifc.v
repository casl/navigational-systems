`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 21.06.2017 15:16:45 
// Module Name: FIFO_write_ifc
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module FIFO_write#
(parameter id = 0 )
(
input clk, 
input reset, 
input ce,
input [1:0] rd_state,
input [17:0] samples_rem,
input [1:0] id_read,
input track_failed,
input Sat_status,
output FIFO_wr_en,
output start_logging_in_fifo);

reg state;

always @(posedge clk)
begin
	if(reset)
		state <= 0;
	else begin
		case(state)
		0:
		if(start_logging_in_fifo) 
		begin	
			state<=1;
		end
		1:
		if(track_failed || ~Sat_status)
			state <= 0;
		endcase
	end
end

assign start_logging_in_fifo = rd_state!=0 && samples_rem==4000 && id_read==id;
assign FIFO_wr_en = ce && state == 1;

endmodule
