`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 21.06.2017 15:33:57 
// Module Name: mux
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module mux(
input [15:0] I_mem,
input [15:0] Q_mem,
input [15:0] I_fifo,
input [15:0] Q_fifo,
input stb_mem,
input stb_fifo,
input sel,
output [15:0] I,
output [15:0] Q,
output stb);

assign I = sel ? I_mem: I_fifo;
assign Q = sel ? Q_mem: Q_fifo;
assign stb = sel ? stb_mem : stb_fifo;

endmodule
