`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.07.2017 23:48:17
// Design Name: 
// Module Name: IQ_Samples_FIFO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module IQ_Samples_FIFO#(
parameter count=60000)
(
input clk,
input reset,
input Empty,
input Sat_Status,
input [15:0] Data_count,
output reg State);

initial State=0;
always@ (posedge clk)
if (reset||Sat_Status)
State<=0;
else
if(State ==0  && Data_count==count)
State<=1;


endmodule