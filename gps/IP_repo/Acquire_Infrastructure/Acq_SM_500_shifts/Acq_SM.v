`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/29/2017 04:40:29 PM
// Design Name: 
// Module Name: Acq_SM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Acq_SM#
(parameter clks_per_iter_log2=10,
parameter clks_per_iter=1000)
(clk,resetn,
next_id,log_complete,chunk_in,
SAT_complete,
prn_in,Acq_reset,
pointer_in,pointer_out,
State,DataReady_DataTaken,chunk);
input clk,resetn,log_complete;
input chunk_in;
input [5:0] next_id;

input [clks_per_iter_log2:0] pointer_in;

input SAT_complete;

output reg [5:0] prn_in;
output [clks_per_iter_log2+1:0] pointer_out;
output Acq_reset;

assign pointer_out = chunk ? pointer_in+2*clks_per_iter : pointer_in;

output reg [1:0] State;
always @(posedge clk)
if(~resetn)
    State<=0;
else
begin
    case(State)
    0:                      //Wait
    if(DataReady_DataTaken)
        State<=1; 
    1:
        State<=2;
    2:
        State<=3;        
    3:                     //Process
    if(SAT_complete)
        State<=0;           
    endcase
end          

assign Acq_reset = State != 3;

output reg DataReady_DataTaken;
always @(posedge clk)
if(log_complete)        //DataReady
DataReady_DataTaken<=1;
else if(State==2) //DataTaken
DataReady_DataTaken<=0;

//Buffer chunk and Sat_id on occurence of log_complete
always @(posedge clk)
if(log_complete)
prn_in<=next_id;

output reg chunk;
always @(posedge clk)
if(log_complete)
chunk<=chunk_in;

endmodule