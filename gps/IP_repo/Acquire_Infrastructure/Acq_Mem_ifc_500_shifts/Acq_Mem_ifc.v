

module Acq_Mem_ifc#
(parameter samples_per_ms=4000)
(clk,ce,resetn,
state,count,log_complete,chunk,
Acq_Mem_addr,
en0,en1,en2,en3);
input clk,ce,resetn;
input [1:0] state;
input [31:0] count;
input log_complete;
wire [13:0] Mem_addr;
wire [1:0] inp;
assign Mem_wr_en = state==1 ;

output reg chunk;
always@(posedge clk)
if(~resetn)
chunk<=0;
else if(log_complete)
chunk<=~chunk;

assign Mem_addr = chunk==1 ? count+(2*samples_per_ms) : count;
output [11:0] Acq_Mem_addr;
output en0,en1,en2,en3;
assign Acq_Mem_addr=Mem_addr[13:2];
assign inp = Mem_addr[1:0];
assign valid = state==1 && ce;
assign en0 = (inp==0) && valid;
assign en1 = (inp==1) && valid;
assign en2 = (inp==2) && valid;
assign en3 = (inp==3) && valid;

endmodule
