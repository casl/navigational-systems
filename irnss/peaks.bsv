package peaks;
	import BRAMCore ::*;
	import FixedPoint ::*;
	import Vector ::*;
	// import BRAMA_modified ::*;

	Vector#(100, FixedPoint#(8,32)) corr_data = replicate(0);

	function Bool inrange(Int#(32) range, Int#(32) i, Int#(32) j);
		Bool status;
		if(abs(i-j)<=range)
			status = True;
		else 
			status = False;
		return status;
	endfunction


	function Int#(32) firstMaxIndex (Vector#(100, FixedPoint#(8,32)) data_vector);
		FixedPoint#(8,32) maxx = 0;
		Int#(32) index = 0;
		for (Int#(32) i = 0; i < 100; i = i + 1)
		begin
			if(data_vector[i] >= maxx)
			begin
				maxx = data_vector[i];
				index = i;
			end
		end
		return index;
	endfunction

	function Int#(32) secondMaxIndex (Int#(32) range, Int#(32) max1, Vector#(100, FixedPoint#(8,32)) data_vector);
		FixedPoint#(8,32) maxx = 0;
		Int#(32) index = 0;
		for (Int#(32) i = 0; i < 100; i = i + 1)
		begin
			if(data_vector[i] > maxx && inrange(range,max1,i) == False)
			begin
				maxx = data_vector[i];
				index = i;
			end
		end
		return index;
	endfunction

	interface Peaks_interface;
		method Action getPeaks();
		method Action initialize(Vector#(100, FixedPoint#(8,32)) data);
		method Action give();
	endinterface

	module compare_peaks(Peaks_interface);
		Reg#(FixedPoint#(8,32)) max1_current <- mkReg(0);
		Reg#(FixedPoint#(8,32)) max2_current <- mkReg(0);
		Reg#(FixedPoint#(8,32)) max1_previous <- mkReg(0);
		Reg#(FixedPoint#(8,32)) max2_previous <- mkReg(0);

		Reg#(Int#(32)) m1c_index <- mkReg(0);
		Reg#(Int#(32)) m2c_index <- mkReg(0);
		Reg#(Int#(32)) m1p_index <- mkReg(0);
		Reg#(Int#(32)) m2p_index <- mkReg(0);
		Reg#(Int#(32)) temp <- mkReg(0); 
		
		rule r1;
			m1c_index <= firstMaxIndex(corr_data); 
			m2c_index <= secondMaxIndex(10, m1c_index, corr_data);
		endrule 

		method Action initialize (Vector#(100, FixedPoint#(8,32)) data);
			Reg#(FixedPoint#(8,32)) u <- mkReg(1);
			for(Integer k = 0; k < 100; k = k + 1)
			begin
				data[k] = u + 1;	
				u <= u + 1;
				$display("Data: %d",k);
			end
			data[80] = 250;
			data[50] = 200; 
		endmethod

		method Action getPeaks();
			m1c_index <= max(m1c_index,m1p_index);
			temp <= max(m2c_index,m2p_index);
			
			if(inrange(10,m1c_index,m1p_index) == True)
				m2c_index <= temp;
			else if(inrange(10,m1c_index,m1p_index) == False)
				m2c_index <= max(temp, min(m1c_index,m1p_index));
			// return {m1c_index,m2c_index,m1p_index,m2p_index};
		endmethod

		method Action give();
	 		max1_current <= corr_data[m1c_index];
	 		max2_current <= corr_data[m2c_index];
	 		max1_previous <= corr_data[m1p_index];
	 		max2_previous <= corr_data[m2p_index];
	 	endmethod
	endmodule 

	module mkTestbench(Empty);
		Peaks_interface m <- compare_peaks;
		Vector#(100, FixedPoint#(8,32)) data = replicate(0);
		Reg#(Bit#(7)) i <- mkReg(0);
		rule s1(i<100);
			m.initialize(data); 
			i <= 100;
		endrule
		rule s2(i==100);
			fxptWrite(3,data[80]);
			$display("\n");
			fxptWrite(3,data[2]);
			$finish(0);
		endrule
	endmodule
endpackage