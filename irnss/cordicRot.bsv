package cordicRot;
	import initial_Settings::*;
	import Real::*;
	import FixedPoint::*;
	import Vector::*;
	import BRAMCore::*;

	interface CordicRotIfc;
		method FixedPoint#(16,16) cosine();
		method FixedPoint#(16,16) sine();
		method Action readIn(FixedPoint#(9,16) arg,Int#(16) signal);
	endinterface

	module mkCordic(CordicRotIfc);
		Reg#(FixedPoint#(16,8)) k <- mkReg(0.6072529350088812561694);

		Reg#(FixedPoint#(16,16)) x <- mkReg(0);
		Reg#(FixedPoint#(16,16)) y <- mkReg(0);
		Reg#(FixedPoint#(9,16)) z <- mkReg(0);
		Reg#(Bit#(2)) d <- mkReg(1);

		Vector#(16,Reg#(FixedPoint#(9,16))) phi <- replicateM(mkReg(0)); 
		Reg#(FixedPoint#(16,16)) temp1 <- mkReg(0);
		Reg#(FixedPoint#(16,16)) temp2 <- mkReg(0);
		Reg#(FixedPoint#(9,16)) temp3 <- mkReg(0);

		Reg#(Bit#(2)) flag <- mkReg(-1);
		Reg#(Bit#(8)) count <- mkReg(0);

		rule rotate(count < 16 && flag == 0);
			// $write("Rotate Iteration %d. x%d = ",count,count);
			// fxptWrite(4,x);
			// $write(", y%d = ",count);
			// fxptWrite(4,y);
			// $write(", z%d = ",count);
			// fxptWrite(4,z);
			// $display("");
			if(d == 1)
			begin
				temp1 <= x - (y >> count);
				temp2 <= y + (x >> count);
				temp3 <= z - phi[count];
			end
			else if(d == -1)
			begin
				temp1 <= x + (y >> count);
				temp2 <= y - (x >> count);
				temp3 <= z + phi[count];
			end
			flag <= 1;
		endrule

		rule store(count < 16 && flag == 1);
			//  $write("Store Iteration %d. x%d = ",count,count+1);
			//  fxptWrite(4,temp1);
			//  $write(", y%d = ",count+1);
			//  fxptWrite(4,temp2);
			//  $write(", z%d = ",count+1);
			//  fxptWrite(4,temp3);
			//  $display("\n");
			x <= temp1;
			y <= temp2;
			z <= temp3;
			if(temp3 < 0)
				d <= -1;
			else
				d <= 1;
			//phi <= phi >> 1;
			count <= count+1;
			flag <= 0;

		endrule

		method FixedPoint#(16,16) cosine() if(count >= 16);
			return x;
		endmethod
		method FixedPoint#(16,16) sine() if(count >= 16);
			return y;
		endmethod
		method Action readIn(FixedPoint#(9,16) arg,Int#(16) signal);
			z <= arg;
			x <= 0.6072529350088812561694*fromInt(signal);
			y <= 0;
			//k <= k*fromInt(signal);
			phi[0]  <= 0.785398163397448;
			phi[1]  <= 0.463647609000806;
			phi[2]  <= 0.244978663126864;
			phi[3]  <= 0.124354994546761;
			phi[4]  <= 0.062418809995957;
			phi[5]  <= 0.031239833430268;
			phi[6]  <= 0.015623728620477;
			phi[7]  <= 0.007812341060101;
			phi[8]  <= 0.003906230131967;
			phi[9]  <= 0.001953122516479;
			phi[10] <= 0.000976562189559;
			phi[11] <= 0.000488281211195;
			phi[12] <= 0.000244140620149;
			phi[13] <= 0.000122070311894;
			phi[14] <= 0.000061035156174;
			phi[15] <= 0.000030517578116;

			count <= 0;
			flag <= 0;
		endmethod

	endmodule // mkCordic


	module mkTb();
		CordicRotIfc ifc <- mkCordic;
		Reg#(Bit#(2)) flag <- mkReg(0);
		Reg#(Bit#(2)) quad <- mkReg(0);
		Reg#(Bit#(1)) sign <- mkReg(0);
		Reg#(FixedPoint#(9,16)) angle <- mkReg(0.172329969285815);

		rule x1 (flag == 0);
			let temp = angle;
			let tempQ = 0;
			if(temp < 0)
			begin
				$display("Neg");
				temp = -temp;
				sign <= 1;
			end
			else if(temp > 1.570796326794897 && temp < 3.141592653589793)
			begin
				$display("Q2");
				temp = temp - 1.570796326794897;
				tempQ = 1;
			end
			else if(temp >= 3.141592653589793 && temp < 4.712388980384690)
			begin
				$display("Q3");
				temp = temp - 3.141592653589793;
				tempQ = 2;
			end

			else if(temp >= 4.712388980384690 && temp < 6.283185307179586)
			begin
				$display("Q4");
				temp = temp - 4.712388980384690;
				tempQ = 3;
			end
			else if(temp >= 6.283185307179586)
			begin
				$display("Greater than 2pi");
				temp = temp - 6.283185307179586;
			end
			angle <= temp;
			quad <= tempQ;

			if(temp >= 0 && temp <= 1.570796326794897)
			begin
				$display("Q1");
				flag <= 1;
			end
		endrule
		rule x2 (flag == 1);
			ifc.readIn(angle,1);
			flag <= 2;
		endrule
		rule x3	(flag == 2);
			let sin = ifc.sine();
			let cos = ifc.cosine();
			if(sign == 1)
				sin = -sin;

			if(quad == 1)
			begin
				let temp = cos;
				cos = -sin;
				sin = temp;
			end
			else if(quad == 2)
			begin
				sin = -sin;
				cos = -cos;
			end
			else if(quad == 3)
			begin
				let temp = sin;
				sin = -cos;
				cos = temp;
			end
			$write("Sine = ");
			fxptWrite(8,sin);
			$write(", Cosine = ");
			fxptWrite(8,cos);
			$finish(0);
		endrule
	endmodule // mkTb
endpackage : cordicRot