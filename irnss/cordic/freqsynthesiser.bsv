//This package gives the base angle and the interval angle which has to be added every time in the angle accumulator
//Input : Sampling frequency, Doppler frequency shift interval, base frequency(the base doppler frequency such as -10 KHz for acquisition) and the phase to be added if any
//Output : Base angle in scalled version of Base frequency and the interval angle in scalled version of Doppler frequency interval

//1. The scaling factor is left shifted by 31 to make it an Integer (from Fixed Point) so as to avoid fixed point computations thereafter.
//2. We do not actually compute (2*pi*fd/fs). Angle lookup happens in degrees, and not radians. Therefore, we need to compute (2*pi*fd/fs)*(180/pi)= (2*fd*180/fs). Also,
//   since there is a scaling factor, where 360 degrees corresponds to 2^32 => 180 degrees corresponds to 2^31, we need to compute (2*fd*180/fs)*(2^31)/180 = (2*fd/fs)*(2^31)= (fd/fs)<<32
//3. TODO instead of computing fd/fs, store 1/fs which you can use for multiplication with fd later. This would save cycles, and also when sampling freq. is a parameter it would save a divider
//4. TODO change read_flag<=0 update to the previous rule. By doing this, we can take in new inputs one cycle earlier.

package freqsynthesiser;

	import Real::*;
	import FixedPoint::*;
	import Vector::*;
	import BRAMCore::*;
	
	interface Ifc_freqsynthesiser;
	
		//interval_in: Doppler freq interval. For acquisition, interval is 500Hz for now.
		//samp_freq_in(fs): 12MHz(value is 12 x 10^6). Usually >10MHz. Fixed for a design. May be can be changed to a parameter rather than an input.
		//base_freq_in: Starting freq. For acquisition, it is -10kHz. 
		//phase_in: Phase to be added. In acquisition, it is 0. In tracking, phase is provided by the PLL.
		method Action get_inputs(Int#(16) interval_in, Int#(32) samp_freq_in,FixedPoint#(16,32) base_freq_in, Int#(33) phase_in);
		method Int#(33) results_base();			//Scaled version of base freq. Scaled by ((2*pi/fs)<<31)
		method Int#(33) results_interval();		//Scaled version of the interval_in.
		
	endinterface : Ifc_freqsynthesiser
	
	module mkfreqsynthesiser(Ifc_freqsynthesiser);
	
		Reg#(Bit#(1)) readFlag <- mkReg(0);
		Reg#(Bit#(3)) flag <- mkReg(0);
	
		Reg#(Int#(16)) interval <- mkReg(0);
		Reg#(FixedPoint#(16,32)) base_freq <- mkReg(0);
		Reg#(Int#(32)) samp_freq <- mkReg(0);
		Reg#(FixedPoint#(16,32)) frq2 <- mkReg(0);
		Reg#(FixedPoint#(16,32)) frq3 <- mkReg(0);
		Reg#(Int#(33)) phase <- mkReg(0);//Scalled version in degrees
		Reg#(Int#(33)) interval_angle <- mkReg(0);
		Reg#(Int#(33)) base_angle <- mkReg(0);

		rule r1_freq_calc((readFlag==1)&&(flag==0));//Calculation of 1/sampling_frequency
			FixedPoint#(24,1) d1 = 1;
			FixedPoint#(32,1) d2 = fromInt(samp_freq);
			FixedPoint#(26,32) d3 = fxptQuot(d1,d2);
			frq2 <= fxptTruncate(d3);
			flag <= 1;
		endrule
		
		rule r2_angle_calc(flag == 1);//Calculation of 2*(f/fs)
			FixedPoint#(16,32) a = frq2;
			frq2 <= 2*base_freq*a;
			frq3 <= 2*fromInt(interval)*a;	
			flag <= 2;
		endrule
		
		rule r3_angle_scalling(flag == 2);//angle scalling
			FixedPoint#(33,1) t1 = 2147483648;
			FixedPoint#(3,32) t2 = fxptTruncate(frq2);
			FixedPoint#(3,32) t3 = fxptTruncate(frq3);
			Int#(36) t4 = fxptGetInt(fxptMult(t1,t2));
			Int#(36) t5 = fxptGetInt(fxptMult(t1,t3));
			Int#(33) a = truncate(t4)+phase;
			Int#(33) b = truncate(t5);
			//This is done to make the angle a positive value between 0 to 2pi. Note that it is not same as taking 2's complement.
			//For example, -pi/2 should become 3pi/2 and not +pi/2.
			//To make the angle a postive value between 0 to 2pi, we add (2^32)-1 i.e. 2pi radians to it.
			//TODO instead add (2^32) which implies that it is enough if you send in the lower 32 bits with the 33rd bit set as 0.
			if(a<0)					
				a=a+4294967295;
			base_angle <= a;
			interval_angle <= b;
			flag <= 3;
			readFlag <= 0;
		endrule
		
		method Action get_inputs(Int#(16) interval_in, Int#(32) samp_freq_in, FixedPoint#(16,32) base_freq_in, Int#(33) phase_in) if(readFlag == 0);
			interval <= interval_in;
			samp_freq <= samp_freq_in;
			base_freq <= base_freq_in;
			phase <= phase_in;
			flag <= 0;
			readFlag <= 1;
		endmethod
		method Int#(33) results_base() if(flag == 3);
			return base_angle;
		endmethod
		method Int#(33) results_interval() if(flag == 3);
			return interval_angle;
		endmethod
	endmodule : mkfreqsynthesiser
endpackage