//Assumption : total number of samples is a multiple of 4
package multadd;

    import BRAM::*;
    import GetPut::*;
    import ClientServer::*;
    import BRAMCore::*;
    import FixedPoint::*;
    import FIFO::*;
    import SpecialFIFOs::*;
    import Vector::*;

    interface Ifc_multadd;
        method Action get_codeshift(Int#(22) early_shift,Int#(22) code_shift,Int#(22) late_shift);
        method Action get_cordic_values(Int#(30) cosine, Int#(30) sine);
        method Int#(32) result_ie;
        method Int#(32) result_qe;
        method Int#(32) result_ip;
        method Int#(32) result_qp;
        method Int#(32) result_il;
        method Int#(32) result_ql;
    endinterface : Ifc_multadd

    typedef 12 Samples;
    typedef 3 Iteration;//Total number of samples/4

    module mkmultadd(Ifc_multadd);

        Integer prn = 1023;
        Integer samp1 = valueOf(Samples);
        Bit#(22) samp = fromInteger(samp1);
        FixedPoint#(12,10) const_1 = 0.0852;

        Reg#(Int#(32)) rg_IE <- mkReg(0);
        Reg#(Int#(32)) rg_QE <- mkReg(0);
        Reg#(Int#(32)) rg_IP <- mkReg(0);
        Reg#(Int#(32)) rg_QP <- mkReg(0);
        Reg#(Int#(32)) rg_IL <- mkReg(0);
        Reg#(Int#(32)) rg_QL <- mkReg(0);

        Reg#(FixedPoint#(12,10)) rg_temp_early <- mkReg(0);
        Reg#(FixedPoint#(12,10)) rg_temp_prompt <- mkReg(0);
        Reg#(FixedPoint#(12,10)) rg_temp_late <- mkReg(0);
        Reg#(Bit#(22)) rg_count1 <- mkReg(0);
        Reg#(Bit#(22)) rg_count2 <- mkReg(0);
        Reg#(Bit#(1)) rg_prompt_prn <- mkReg(0);
        Reg#(Bit#(1)) rg_early_prn <- mkReg(0);
        Reg#(Bit#(1)) rg_late_prn <- mkReg(0);
        Reg#(Bit#(1)) rg_prompt_set <- mkReg(0);

        Vector#(3,FIFO#(Tuple2 #(Int#(30),Int#(30)))) pipe <- replicateM(mkFIFO);

        BRAM_DUAL_PORT#(Int#(11),Bit#(1)) prn_val <- mkBRAMCore2Load(prn,False,"/home/sadhana/Desktop/NavIC_correlator/src/prn_value",False);

        rule r1_compute_prn_bram_address;
            Int#(11) address_e = truncate(fxptGetInt(rg_temp_early));
                Int#(11) address_p = truncate(fxptGetInt(rg_temp_prompt));
                    Int#(11) address_l = truncate(fxptGetInt(rg_temp_late));
            prn_val.a.put(False,address_e,?);
                prn_val.b.put(False,address_l,?);
            if(address_e == address_p)
                rg_prompt_set <= 0;
            else if(address_l == address_p)
                rg_prompt_set <= 1;
            rg_temp_early <= rg_temp_early + const_1;
                rg_temp_prompt <= rg_temp_prompt + const_1;
                     rg_temp_late <= rg_temp_late + const_1;
            //rg_count1 <= rg_count1 + 1;
            //$display($time," %d: r1_compute_prn_bram_address : addresses: e=%d p=%d l=%d \n",rg_count1,address_e,address_p,address_l);
            //match{.x1,.y1} = pipe[0].first; pipe[0].deq;
            pipe[1].enq(pipe[0].first);pipe[0].deq;
        endrule
        rule r2_prn_from_bram;
            let early_prn = prn_val.a.read;
                let late_prn = prn_val.b.read;
            rg_early_prn <= early_prn;
                rg_late_prn <= late_prn;
            if(rg_prompt_set == 0)
                rg_prompt_prn <= early_prn;
            else
                rg_prompt_prn <= late_prn;
            //$display($time,"  %d : r2_prn_from_bram : e=%d l=%d  %d \n",rg_count1,early_prn,late_prn,rg_prompt_set);
            //match{.x2,.y2} = pipe[1].first; pipe[1].deq;
            pipe[2].enq(pipe[1].first); pipe[1].deq;
        endrule
        rule r3_decoder;
            match{.x3,.y3} = pipe[2].first; pipe[2].deq;
            if(rg_early_prn == 1)
            begin
                rg_IE <= rg_IE + signExtend(x3);
                rg_QE <= rg_QE + signExtend(y3);
            end
            else
            begin
                rg_IE <= rg_IE - signExtend(x3);
                rg_QE <= rg_QE - signExtend(y3);
            end
            if(rg_prompt_prn == 1)
            begin
                rg_IP <= rg_IP + signExtend(x3);
                rg_QP <= rg_QP + signExtend(y3);
            end
            else
            begin
                rg_IP <= rg_IP - signExtend(x3);
                rg_QP <= rg_QP - signExtend(y3);
            end
            if(rg_late_prn == 1)
            begin
                rg_IL <= rg_IL + signExtend(x3);
                rg_QL <= rg_QL + signExtend(y3);
            end
            else
            begin
                rg_IL <= rg_IL - signExtend(x3);
                rg_QL <= rg_QL - signExtend(y3);
            end
            rg_count2 <= rg_count2 + 1;
            $display($time,"  %d : previous sum : IE=%d QE=%d \n",rg_count2,rg_IE,rg_QE);
        endrule

        method Action get_codeshift(Int#(22) early_shift,Int#(22) code_shift,Int#(22) late_shift);
            //$display($time," Started method get_codeshift \n");
            FixedPoint#(22,10) temp_e1 = fromInt(early_shift);
                FixedPoint#(22,10) temp_p1 = fromInt(code_shift);
                    FixedPoint#(22,10) temp_l1 = fromInt(late_shift);
            rg_temp_early <= fxptTruncate(fxptMult(temp_e1,const_1));
                rg_temp_prompt <= fxptTruncate(fxptMult(temp_p1,const_1));
                    rg_temp_late <= fxptTruncate(fxptMult(temp_l1,const_1));
            rg_IE <= 0;rg_QE <= 0;
                rg_IP <= 0;rg_QP <= 0;
                    rg_IL <= 0;rg_QL <= 0;
            rg_count2 <= 0;
        endmethod
        method Action get_cordic_values(Int#(30) cosine, Int#(30) sine);
            pipe[0].enq(tuple2(cosine,sine));
            $display($time," %d : Obtained cordic values : %d %d ",rg_count1,cosine,sine);
        endmethod
        method Int#(32) result_ie if(rg_count2 == samp);
            return rg_IE;
        endmethod
        method Int#(32) result_qe if(rg_count2 == samp);
            return rg_QE;
        endmethod
        method Int#(32) result_ip if(rg_count2 == samp);
            return rg_IP;
        endmethod
        method Int#(32) result_qp if(rg_count2 == samp);
            return rg_QP;
        endmethod
        method Int#(32) result_il if(rg_count2 == samp);
            return rg_IL;
        endmethod
        method Int#(32) result_ql if(rg_count2 == samp);
            return rg_QL;
        endmethod
    endmodule : mkmultadd
endpackage : multadd