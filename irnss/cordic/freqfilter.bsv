//Second order frequency filter after frequency discriminator in FLL
//Input : Previous two frequencies from frequency discriminator and the previous carrier frequency updated in the last tracking iteration
//Output: Carrier frequency to the nco for the next tracking iteration
package freqfilter;

import Real::*;
import FixedPoint::*;

interface Ifc_freqfilter;
    method Action get_inputs(FixedPoint#(16,32) freq_in1, FixedPoint#(16,32) freq_in2);
    method FixedPoint#(16,32) results();
endinterface : Ifc_freqfilter

module mkfreqfilter(Ifc_freqfilter);

    Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
    Reg#(FixedPoint#(16,32)) rg_kf1 <- mkReg(0.0305);
    Reg#(FixedPoint#(16,32)) rg_kf2 <- mkReg(0.695);
    Reg#(FixedPoint#(16,32)) rg_freqout <- mkReg(0);

    method Action get_inputs(FixedPoint#(16,32) freq_in1, FixedPoint#(16,32) freq_in2) if(rg_readFlag == 0);
        FixedPoint#(16,32) temp = (fxptZeroExtend(rg_kf1)*freq_in1)+(fxptZeroExtend(rg_kf2)*freq_in2);
        rg_freqout <= temp;
        rg_readFlag <= 1;
    endmethod
    method FixedPoint#(16,32) results() if(rg_readFlag == 1);
        return rg_freqout;
    endmethod
endmodule : mkfreqfilter

endpackage : freqfilter