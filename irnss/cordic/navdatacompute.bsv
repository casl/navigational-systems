package navdatacompute;

    import Real::*;
    import BRAM::*;
    import BRAMCore::*;
    import ServerClient::*;
    import GetPut::*;

    import trackingfsm::*;

    interface Ifc_navdatacompute;
        method Action getinput(Bit#(20) loopcnt);
        method Bit#(1) result();
    endinterface : Ifc_navdatacompute

    module mknavdatacompute(Ifc_navdatacompute);

        Reg#(Bit#(20)) rg_loopcnt <- mkReg(0);
        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
        Reg#(Bit#(1)) rg_nav_data <- mkReg(0);
        Reg#(Bit#(6)) rg_count <- mkReg(0);
        Reg#(Bit#(6)) rg_start <- mkReg(0);
        Reg#(Bit#(5)) rg_count_zero <- mkReg(0);
        Reg#(Bit#(5)) rg_count_one <- mkReg(0);

        Ifc_trackingfsm ifc1 <- mktrackingfsm;

        rule r1_calculate_start_address(rg_readFlag == 1);
            if(rg_start <20)
                rg_count <= rg_start + 20;
            else
                rg_count <= rg_start - 20;
            rg_readFlag <= 2;
        endrule
        rule r2_readvalue_from_bram(rg_count != rg_start);
            ifc1.readbram(rg_count);
            if(rg_readFlag == 2)
                rg_readFlag <= 3;
            if(rg_count == 39)
                rg_count <= 0;
            else
                rg_count <= rg_count + 1;
        endrule
        rule r3_get_values_from_bram(rg_readFlag == 3);
            let ip <- ifc1.bramip;
            if(pack(ip)[63] == 0)
                rg_count_zero <= rg_count_zero+1;
            else
                rg_count_one <= rg_count_one+1;
            if(rg_count == rg_start)
                rg_readFlag <= 4;
        endrule
        rule r4_nav_bit_compute(rg_readFlag == 4);
            if(rg_count_zero > rg_count_one)
                rg_nav_data <= 0;
            else
                rg_nav_data <= 1;
            rg_readFlag <= 5;
        endrule
    
        method Action getinput(Bit#(20) loopcnt);
            Bit#(6) temp = loopcnt%40;
            rg_count <= temp;
            rg_start <= temp;
            rg_count_one <= 0;
            rg_count_zero <= 0;
            rg_readFlag <= 1;
        endmethod
        method Bit#(1) result() if(rg_readFlag == 5);;
            return rg_nav_data;
        endmethod
    endmodule : mknavdatacompute

endpackage : navdatacompute