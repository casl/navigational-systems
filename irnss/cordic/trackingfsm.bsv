package trackingfsm;

import freqsynthesiser::*;
import cordicpipe::*;
import multadd::*;
import arctan::*;
import phasefilter::*;
import freqdiscriminator::*;
import freqfilter::*;
import codediscriminator::*;

import Real::*;
import FixedPoint::*;
import Vector::*;
import StmtFSM::*;
import BRAM::*;
import BRAMCore::*;
import ClientServer::*;
import GetPut::*;

typedef 12 Samples;//samples considered for every 1ms
typedef 500 Interval;
typedef 12000000 Sampling_frequency;
typedef 10000 Stop_frequency;
typedef 500 Tracking_iterations;
typedef 5 Half_samples_per_code_chip;

interface Ifc_trackingfsm;
    method Action acq_inputs(FixedPoint#(16,32) carr_freq_in, FixedPoint#(16,32) dopp_freq_in, Bit#(22) code_shift_in, Int#(33) phase_in, Bit#(20) loopcnt);
    method Action readbram(Bit#(6) address);
    method ActionValue#(Int#(32)) bramip;
    method ActionValue#(Int#(32)) bramqp;
    method FixedPoint#(16,32) carr_freq_out;
    method FixedPoint#(16,32) dopp_freq_out;
    method Bit#(22) code_shift_out;
    method Int#(33) phase_out;
    method Bit#(1) sign_change;
endinterface : Ifc_trackingfsm

function BRAMRequest#(Bit#(6), Int#(32)) makeRequest(Bool write, Bit#(6) addr, Int#(32) data);
    return BRAMRequest{
    write: write,
    responseOnWrite:False,
    address: addr,
    datain: data
    };
endfunction

(*synthesize*)
module mktrackingfsm(Ifc_trackingfsm);
    Integer samp1 = valueOf(Samples);
    Bit#(22) samp = fromInteger(samp1);
    Integer interval1 = valueOf(Interval);
    Int#(16) interval = fromInteger(interval1);
    Integer samp_freq1 = valueOf(Sampling_frequency);
    Int#(32) samp_freq = fromInteger(samp_freq1);
    Integer stop_freq = valueOf(Stop_frequency);
    Int#(16) stop = fromInteger(stop_freq);
    Integer samplepercodechip = valueOf(Half_samples_per_code_chip);
    Bit#(5) spcc = fromInteger(samplepercodechip);

    Reg#(FixedPoint#(16,32)) rg_carr_freq <- mkReg(0);
    Reg#(FixedPoint#(16,32)) rg_carr_freqdev1 <- mkReg(0);
    Reg#(FixedPoint#(16,32)) rg_carr_freqdev2 <- mkReg(0);
    Reg#(FixedPoint#(16,32)) rg_dopp_freq <- mkReg(0);
    Reg#(Bit#(22)) rg_code_shift <- mkReg(0);
    Reg#(Int#(33)) rg_phase <- mkReg(0);
    Reg#(Int#(33)) rg_phasedev1 <- mkReg(0);
    Reg#(Int#(33)) rg_phasedev2 <- mkReg(0);
    Reg#(Int#(33)) rg_lastphase <- mkReg(0);
    Reg#(Int#(32)) rg_IE <- mkReg(0);
    Reg#(Int#(32)) rg_QE <- mkReg(0);
    Reg#(Int#(32)) rg_IP <- mkReg(0);
    Reg#(Int#(32)) rg_QP <- mkReg(0);
    Reg#(Int#(32)) rg_IL <- mkReg(0);
    Reg#(Int#(32)) rg_QL <- mkReg(0);
    Reg#(Int#(32)) rg_E <- mkReg(0);
    Reg#(Int#(32)) rg_P <- mkReg(0);        //TODO remove this as it is not used in code discriminator
    Reg#(Int#(32)) rg_L <- mkReg(0);
    Reg#(Bool) rg_odd <- mkReg(True);
    Reg#(Bit#(1)) rg_sign_change <- mkReg(0);
    Reg#(Bit#(20)) rg_loopcnt <- mkReg(0);

    Reg#(Bit#(1)) readFlag <- mkReg(0);
    Reg#(Bit#(1)) flag <- mkReg(0);
    Reg#(Bit#(22)) i <- mkReg(0);

    Ifc_multadd multadd <- mkmultadd;
    Ifc_freqsynthesiser freqsynthesiser <- mkfreqsynthesiser;
    Ifc_cordicpipe cordicpipe <- mkcordicpipe;
    Ifc_arctan arctan_a <- mkarctan;
    Ifc_arctan arctan_b <- mkarctan;
    Ifc_arctan arctan_c <- mkarctan;
    Ifc_phasefilter phasefilter <- mkphasefilter;
    Ifc_freqdiscriminator freqdiscriminator <- mkfreqdiscriminator;
    Ifc_freqfilter freqfilter <- mkfreqfilter;
    Ifc_codediscriminator codediscriminator <- mkcodediscriminator;

    BRAM_Configure cfg1 = defaultValue;
    BRAM1Port#(Bit#(6),Int#(32)) ip <- mkBRAM1Server(cfg1);
    BRAM1Port#(Bit#(6),Int#(32)) qp <- mkBRAM1Server(cfg1);
    Reg#(Bit#(6)) count1 <- mkReg(0);

    Stmt track_stmt = 
    (seq
        par
            action
                Int#(22) e = unpack(rg_code_shift - zeroExtend(spcc)); //Calculation of Early code shift
                Int#(22) l = unpack(rg_code_shift + zeroExtend(spcc)); //Calculation of Late code shift
                multadd.get_codeshift(e,unpack(rg_code_shift),l);
            endaction
            seq
                freqsynthesiser.get_inputs(0,samp_freq,rg_carr_freq,rg_phase);
                action // Getting the scalled version of carrier frequency
                    Int#(33) a = freqsynthesiser.results_base();
                    cordicpipe.initialise(a,rg_loopcnt);
                endaction
                for(i<=0; i<samp; i<=i+1) // to read all the frequency shifted values
                    action
                        let cosine = cordicpipe.cosine();
                        let sine = cordicpipe.sine();
                        $display($time,"coise = %d ans sine = %d",cosine,sine);
                        multadd.get_cordic_values(cosine,sine);
                    endaction
                rg_lastphase <= cordicpipe.lastphase_out;
            endseq
        endpar
        action
        //Results from multadd
            rg_IE <= multadd.result_ie;
            rg_QE <= multadd.result_qe;
            rg_IP <= multadd.result_ip;
            rg_QP <= multadd.result_qp;
            rg_IL <= multadd.result_il;
            rg_QL <= multadd.result_ql;
        endaction
        action
        //Calculation of magnitude and phase functions
        //Inputs to arctan(phase discriminator module)
            arctan_a.get_inputs(rg_IE,rg_QE);
            arctan_b.get_inputs(rg_IP,rg_QP);//Phase discriminator input
            arctan_c.get_inputs(rg_IL,rg_QL);
        endaction
        action
            //Updating values of I_P and Q_P in BRAM
            ip.portA.request.put(makeRequest(True,count1,rg_IP));
            qp.portA.request.put(makeRequest(True,count1,rg_QP));
            if(count1 == 39)
                count1 <= 0;
            else
                count1 <= count1+1;
            //outputs from arctan(phase discriminator module)
            rg_E <= arctan_a.result_mag();
            rg_P <= arctan_b.result_mag();
            rg_L <= arctan_c.result_mag();
            rg_phasedev1 <= arctan_b.result_phase();//Phase discriminator output
        endaction
        action
            $display("\n rg_IE = %d, rg_QE = %d, rg_IP = %d, rg_QP = %d, rg_IL = %d and rg_QL = %d\n",rg_IE,rg_QE,rg_IP,rg_QP,rg_IL,rg_QL);
            $display("\n rg_E = %d, rg_P = %d, rg_L = %d",rg_E,rg_P,rg_L);
        endaction
        par
            seq
                if(rg_odd == True)
                seq
                    action
                        phasefilter.get_inputs(rg_phasedev1,rg_phasedev2);//phase filter input
                        freqdiscriminator.get_inputs(rg_phasedev1,rg_phasedev2,1);//Frequency discriminator input
                    endaction
                    action
                        rg_phasedev1 <= phasefilter.results();//phase filter output
                        rg_carr_freqdev1 <= freqdiscriminator.results();//Frequency discriminator output
                    endaction
                    freqfilter.get_inputs(rg_carr_freqdev1, rg_carr_freqdev2);//Frequency filter input
                    rg_carr_freqdev1 <= freqfilter.results();//Frequency filter output
                    action
                        rg_phase <= rg_phase + rg_phasedev1;//Updating phase
                        rg_carr_freq <= rg_carr_freq + rg_carr_freqdev1;//Updating carrier frequency
                    endaction
                    rg_dopp_freq <= rg_carr_freq;//updating doppler frequency
                    rg_carr_freqdev2 <= rg_carr_freqdev1;
                endseq
                else
                seq
                    rg_phase <= rg_lastphase;
                endseq
            rg_phasedev2 <= rg_phasedev1;
            endseq
            seq
                action
                    Int#(5) factor = unpack(spcc);
                    codediscriminator.get_inputs(rg_E,rg_L,factor);//code discriminator input
                endaction
                action
                    Int#(22) temp = unpack(rg_code_shift);
                    rg_code_shift <= pack(temp - zeroExtend(codediscriminator.result));//code shift error correction from code discriminator
                endaction
            endseq
            rg_sign_change <= rg_sign_change ^ pack(rg_IP)[31];
        endpar
    flag <= 1;
    readFlag <= 0;
    endseq);

    FSM track_fsm <- mkFSM(track_stmt);

    rule r0_initialisefsm((readFlag==1)&&(track_fsm.done));
        track_fsm.start;
    endrule
    
    method Action acq_inputs(FixedPoint#(16,32) carr_freq_in, FixedPoint#(16,32) dopp_freq_in, Bit#(22) code_shift_in, Int#(33) phase_in, Bit#(20) loopcnt) if(readFlag == 0);
        rg_carr_freq <= carr_freq_in;
        rg_dopp_freq <= dopp_freq_in;
        rg_code_shift <= code_shift_in;
        rg_phase <= phase_in;
        rg_sign_change <= pack(rg_IP)[31];
        rg_loopcnt <= loopcnt;
        readFlag <= 1;
        flag <= 0;
        rg_odd <= !rg_odd;
    endmethod
    method Action readbram(Bit#(6) address);
        ip.portA.request.put(makeRequest(False,address,?));
        qp.portA.request.put(makeRequest(False,address,?));
    endmethod
    method ActionValue#(Int#(32)) bramip;
        let ip_val <- ip.portA.response.get;
        return ip_val;
    endmethod
    method ActionValue#(Int#(32)) bramqp;
        let qp_val <- qp.portA.response.get;
        return qp_val;
    endmethod
    method FixedPoint#(16,32) carr_freq_out if(flag == 1);
        return rg_carr_freq;
    endmethod
    method FixedPoint#(16,32) dopp_freq_out if(flag == 1);
        return rg_dopp_freq;
    endmethod
    method Bit#(22) code_shift_out if(flag == 1);
        return rg_code_shift;
    endmethod
    method Int#(33) phase_out if(flag == 1);
        return rg_phase;
    endmethod
    method Bit#(1) sign_change if(flag == 1);
        return rg_sign_change;
    endmethod
endmodule : mktrackingfsm
endpackage : trackingfsm