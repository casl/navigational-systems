package signchangecompute;

    //import trackingfsm::*;
    import testforsignchangecompute::*;
    import BRAM::*;
    import BRAMCore::*;
    import ClientServer::*;
    import GetPut::*;

    interface Ifc_signchangecompute;
        method Action initialise(Bit#(6) mod_loopcnt);
        method Bit#(5) result_signI();
        method Bit#(5) result_signQ();
    endinterface : Ifc_signchangecompute

    module mksignchangecompute(Ifc_signchangecompute);

        Reg#(Bit#(1)) rg_readFlag <- mkReg(0);
        Reg#(Bit#(1)) rg_flag <- mkReg(0);
        Reg#(Bit#(6)) rg_count <- mkReg(0);
        Reg#(Bit#(6)) rg_start <- mkReg(0);
        Reg#(Bit#(1)) rg_signprevI <- mkReg(0);
        Reg#(Bit#(1)) rg_signprevQ <- mkReg(0);
        Reg#(Bit#(1)) rg_signpresentI <- mkReg(0);
        Reg#(Bit#(1)) rg_signpresentQ <- mkReg(0);
        Reg#(Bit#(5)) rg_signchangeI <- mkReg(0);
        Reg#(Bit#(5)) rg_signchangeQ <- mkReg(0);
        
        Ifc_testforsignchangecompute ifc1 <- mktestforsignchangecompute;

        rule r1_giveaddresstoBRAM(rg_count < 20);
            ifc1.readbram(rg_count+rg_start);
            if(rg_count==0)
                rg_readFlag <= 1;
            rg_count <= rg_count+1;
        endrule
        rule r2_getvaluesfromBRAM(rg_readFlag == 1);
            let ip <- ifc1.bramip;
            let qp <- ifc1.bramqp;
            if(pack(ip)[63] == 0)
                rg_signpresentI <= 1;
            else
                rg_signpresentI <= 0;
            if(pack(qp)[63] == 0)
                rg_signpresentQ <= 1;
            else
                rg_signpresentQ <= 0;
            if(rg_count == 20)
                rg_readFlag <= 0;
        endrule
        rule r3_updatepast(rg_readFlag==1);
            rg_signprevI <= rg_signpresentI;
            rg_signprevQ <= rg_signpresentQ;
        endrule
        rule r3_signchangecomputation((rg_count > 2)&&(rg_flag == 0));
            Bit#(1) temp1 = rg_signpresentI^rg_signprevI;
            Bit#(1) temp2 = rg_signpresentQ^rg_signprevQ;
            rg_signchangeI <= rg_signchangeI + zeroExtend(temp1);
            rg_signchangeQ <= rg_signchangeQ + zeroExtend(temp2);
            if(rg_readFlag == 0)
                rg_flag <= 1;
        endrule

        method Action initialise(Bit#(6) mod_loopcnt);
            rg_flag <= 0;
            rg_count <= 0;
            rg_start <= mod_loopcnt;
            rg_signchangeI <= 0;
            rg_signchangeQ <= 0;
        endmethod
        method Bit#(5) result_signI() if(rg_flag == 1);
            return rg_signchangeI;
        endmethod
        method Bit#(5) result_signQ() if(rg_flag == 1);
            return rg_signchangeQ;
        endmethod
    endmodule : mksignchangecompute

endpackage : signchangecompute