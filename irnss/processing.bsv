package processing;
	import initial_Settings::*;
	import FixedPoint ::*;
	import Vector ::*;

	Int#(32) size1 = msToProcess/integrationTime;
	typedef struct 
	{
		//Real[msToProcess/integrationTime] i_P, i_E, i_L, q_E, q_P, q_L, theta, filttheta, freqdev, phi, dopplerFreq, carrFreq, codeFreq, codephase, filtcodephase,codeError, lockCheck, est_CNR, dPRange, data, data_boundary, acq_Skip;
		Vector#(500, Reg#(Int#(16))) i_P; 
		Vector#(500, Reg#(Int#(16))) i_E;
		Vector#(500, Reg#(Int#(16))) i_L;
		Vector#(500, Reg#(Int#(16))) q_E;
		Vector#(500, Reg#(Int#(16))) q_P;
		Vector#(500, Reg#(Int#(16))) q_L;
		Vector#(500, Reg#(Int#(16))) theta;
		Vector#(500, Reg#(Int#(16))) filttheta;
		Vector#(500, Reg#(Int#(16))) freqdev;
		Vector#(500, Reg#(Int#(16))) phi;
		Vector#(500, Reg#(Int#(16))) filtfreqdev;
		Vector#(500, Reg#(Int#(16))) dopplerFreq;
		Vector#(500, Reg#(Int#(16))) carrFreq;
		Vector#(500, Reg#(Int#(16))) codeFreq;
		Vector#(500, Reg#(Int#(16))) codephase;
		Vector#(500, Reg#(Int#(16))) filtcodephase;
		Vector#(500, Reg#(Int#(16))) codeError;
		Vector#(500, Reg#(Int#(16))) lockCheck;
		Vector#(500, Reg#(Int#(16))) est_CNR;
		Vector#(500, Reg#(Int#(16))) dPRange;
		Vector#(500, Reg#(Int#(16))) data;
		Vector#(500, Reg#(Int#(16))) data_boundary;
		Vector#(500, Reg#(Int#(16))) acq_Skip;
	Int#(32) acqTh;
	Int#(32) prn;
	} Track deriving (Bits, Eq);

	module mkstructure(Empty);
		Vector#(numberOfChannels,Reg#(Track)) trackResults;// <- replicateM(replicateM(mkReg(0)));
		Reg#(Bit#(10)) i <- mkReg(0);
		for(i <= 0; i < numberOfChannels; i <= i+1)
		begin
			trackResults[i].i_P  <- replicateM(mkReg(0));
			trackResults[i].i_E  <- replicateM(mkReg(0));
			trackResults[i].i_L  <- replicateM(mkReg(0));
			trackResults[i].q_P  <- replicateM(mkReg(0));
			trackResults[i].q_E  <- replicateM(mkReg(0));
			trackResults[i].q_L  <- replicateM(mkReg(0));

			trackResults[i].theta  <- replicateM(mkReg(0));
			trackResults[i].filttheta  <- replicateM(mkReg(0));
			trackResults[i].freqdev  <- replicateM(mkReg(0));
			trackResults[i].filtfreqdev  <- replicateM(mkReg(0));
			
			trackResults[i].phi  <- replicateM(mkReg(0));
			trackResults[i].dopplerFreq  <- replicateM(mkReg(0));
			trackResults[i].carrFreq  <- replicateM(mkReg(iF));

			trackResults[i].codeFreq  <- replicateM(mkReg(codeFreqBasis));
			trackResults[i].codephase  <- replicateM(mkReg(0));

			trackResults[i].filtcodephase  <- replicateM(mkReg(0));
			trackResults[i].codeError  <- replicateM(mkReg(0));
			
			trackResults[i].lockCheck  <- replicateM(mkReg(0));
			trackResults[i].est_CNR  <- replicateM(mkReg(0));

			trackResults[i].dPRange  <- replicateM(mkReg(0));
			trackResults[i].data  <- replicateM(mkReg(0));
			trackResults[i].data_boundary  <- replicateM(mkReg(0));

			trackResults[i].acq_Skip <- replicateM(mkReg(0));

			trackResults[i].acqTh  <- 0;
			trackResults[i].prn  <- 0;
		end
		
		rule r1(i < 11);
			
		endrule
	endmodule
endpackage: processing