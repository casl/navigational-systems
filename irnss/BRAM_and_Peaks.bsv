package BRAM_and_Peaks;
	import BRAMCore ::*;
	import FixedPoint ::*;
	import Vector ::*;
	import initial_Settings ::*;
	import BuildVector ::*;

	function Bool inrange(Int#(14) range, Int#(14) i, Int#(14) j);
		Bool status;
		if(abs(i-j)<=range)
			status = True;
		else 
			status = False;
		return status;
	endfunction

	interface Inputs;
		method Action fill(Int#(14) i, Int#(SumSize) l);
		method Action reqReadBRAM (Int#(14) k);
		method Int#(SumSize) readfrmBRAM ();
		method Action findMaxMin(Reg#(Int#(14)) count, Vector#(100, Reg#(Int#(SumSize))) p, Reg#(Int#(SumSize)) max1_current,Reg#(Int#(SumSize)) max2_current,Reg#(Int#(14)) m1c_index,Reg#(Int#(14)) m2c_index,Reg#(Int#(SumSize)) max1_previous,Reg#(Int#(SumSize)) max2_previous,Reg#(Int#(14)) callNo);
	endinterface

	module mkBram(Inputs);
		BRAM_PORT#(Int#(14),Int#(SumSize)) spk <- mkBRAMCore1(100,False);
		method Action fill(Int#(14) i, Int#(SumSize) l); 
			spk.put(True,i,l);
		endmethod
		
		method Action reqReadBRAM(Int#(14) k);
			spk.put(False,k,?);
		endmethod

		method Int#(SumSize) readfrmBRAM ();
			Int#(SumSize) x = 0;
			x = spk.read();
			return x;
		endmethod

		method Action findMaxMin(Reg#(Int#(14)) count, Vector#(100, Reg#(Int#(SumSize))) p, Reg#(Int#(SumSize)) max1_current,Reg#(Int#(SumSize)) max2_current,Reg#(Int#(14)) m1c_index,Reg#(Int#(14)) m2c_index,Reg#(Int#(SumSize)) max1_previous,Reg#(Int#(SumSize)) max2_previous,Reg#(Int#(14)) callNo);
			let lv_data = p[count];
			let lv_count = count + 100*(callNo-1);
			let temp = max1_current;
			let temp_index = m1c_index;

			if(lv_data > max1_current)
			begin		
				max1_current <= lv_data;
				m1c_index <= lv_count;
				if(inrange(10,lv_count,m1c_index)==False)
				begin
					max2_current <= temp;
					m2c_index <= temp_index;
				end
			end
			else if(lv_data > max2_current && lv_data < max1_current && lv_data!= max1_previous && lv_data!= max2_previous)
			begin
				if(inrange(10,lv_count,m1c_index) == False)
				begin
					max2_current <= lv_data;
					m2c_index <= lv_count;
				end
			end
		endmethod
	endmodule

	module mkPeaks();
		Inputs m <- mkBram;
		
		Reg#(Int#(14)) count_rand <- mkReg(0);
		Reg#(Int#(14)) count <- mkReg(0);
		Reg#(Int#(14)) j <- mkReg(0);
		Reg#(Int#(14)) callNo <- mkReg(1);
		Reg#(Bit#(2)) k <- mkReg(2);
		Reg#(Bit#(3)) status <- mkReg(0);
		Reg#(Int#(SumSize)) y <- mkReg(1);

		Reg#(Int#(SumSize)) max1_current <- mkReg(0);
		Reg#(Int#(SumSize)) max2_current <- mkReg(0);
		Reg#(Int#(SumSize)) max1_previous <- mkReg(0);
		Reg#(Int#(SumSize)) max2_previous <- mkReg(0);
		
		Reg#(Int#(14)) m1c_index <- mkReg(0);
		Reg#(Int#(14)) m2c_index <- mkReg(0);
		Reg#(Int#(14)) m1p_index <- mkReg(0);
		Reg#(Int#(14)) m2p_index <- mkReg(0);
		Reg#(Int#(14)) temp_index <- mkReg(0); 
		Reg#(Int#(SumSize)) temp <- mkReg(0);
		Vector#(100, Reg#(Int#(SumSize))) data <- replicateM(mkReg(1));

		rule start(j<100 && k==2);
			m.fill(j,y);
			y <= y + 1;
			j <= j + 1;
			if(j>=99) k<=0;
		endrule
		
		rule request(count_rand < 100 && k==0);
			m.reqReadBRAM(count_rand);
			k <= 1;
		endrule 

		rule read(count_rand < 100 && k==1);
			let v = m.readfrmBRAM();
			data[count_rand] <= v;
			if(count_rand==99) k<=3;
			else begin count_rand <= count_rand + 1; k<=0; end
		endrule

		rule assigned(k==3 && status==0);
			max1_previous <= max1_current;
			max2_previous <= max2_current;
			m1p_index <= m1c_index;
			m2p_index <= m2c_index;
			$display("In assigned");
			status <= 1;
			count <= 0;
		endrule

		rule processing(status==1);
			if(count==0) $display("In processing");
			m.findMaxMin(count,data,max1_current,max2_current,m1c_index,m2c_index,max1_previous,max2_previous,callNo);
			count<=count+1;	
			if(count>99)
			begin
				$display("Max1c =%d M1c_index =%d\nMax2c =%d M2c_index =%d\n",max1_current,m1c_index,max2_current,m2c_index);
				$display("Max1p =%d M1p_index =%d\nMax2p =%d M2p_index =%d\nCallNo = %d",max1_previous,m1p_index,max2_previous,m2p_index,callNo);
				$display("------------------------------------------------------------\n");
				status <= 2;
			end
		endrule

		rule compare(status==2);
			$display("In compare");
			max1_current <= max(max1_current,max1_previous);
			temp <= max(max2_current,max2_previous);
			
			if(max1_previous>max1_current)
				m1c_index <= m1p_index;
				
			if(max2_previous>max2_current)
				temp_index <= m2p_index;

			else temp_index <= m2c_index;

			if(inrange(10,m1p_index,m1c_index) == True) 
			begin
				max2_current <= temp;
				m2c_index <= temp_index;
			end

			else 
			begin
				max2_current <= max(temp,min(max1_current,max1_previous));
				
				if(temp>min(max1_current,max1_previous)) m2c_index <= temp_index;
				else
				begin 
					if(max1_previous<max1_current) m2c_index <= m1p_index;
					else m2c_index <= m1c_index; 
				end
			end
			
			if(callNo>=10)begin /*$display("callNo = %d, count = %d\n",callNo,count);*/ $finish(0); end
			callNo <= callNo + 1;
			status <= 0;
		endrule	 
	endmodule
endpackage
	