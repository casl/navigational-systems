package acquireBank;
	import corrBank::*;
	import initial_Settings::*;
	import correlator::*;
	import nco::*;
	import writeData::*;

	import Vector::*;
	import FixedPoint::*;
	import Real::*;
	import BRAMCore::*;

	module mkAcq();
		Int#(8) prn = 0;
		Int#(8) prn_max = 32;

		WriteIFC w1 <- mkWrite();
		CorrBankIfc c1 <-mkBank();

		BRAM_PORT#(Bit#(8),Int#(SumSize)) sums <- mkBRAMCore1(100,False);

		Reg#(Bit#(1)) writeFlag <- mkReg(0);
		Reg#(Bit#(1)) acqflag <- mkReg(0);
		Reg#(Bit#(1)) flag <- mkReg(0);

		Reg#(Bit#(15)) count <- mkReg(0);
		Reg#(Bit#(10)) index <- mkReg(0);
		Vector#(Size,Reg#(Int#(BitSize))) yi = replicateM(mkReg(0));
		Vector#(Size,Reg#(Int#(BitSize))) yq = replicateM(mkReg(0));

		Reg#(Int#(DataSize)) sum <- mkReg(0);
		Reg#(Bit#(14)) loopcount <- mkReg(0);

		rule acqData(writeFlag == 1 && acqflag == 0 && index < Size1);
			signal_I1.put(False,count,?);
			signal_I2.put(False,count,?);
			signal_I3.put(False,count,?);
			signal_I4.put(False,count,?);

			signal_Q1.put(False,count,?);
			signal_Q2.put(False,count,?);
			signal_Q3.put(False,count,?);
			signal_Q4.put(False,count,?);

			acqflag <= 1;
		endrule
		
		rule readData(writeFlag == 1 && acqflag == 1 && index < Size1);
			yi[index] <= signal_I1.read();
			yi[index+1] <= signal_I2.read();
			yi[index+2] <= signal_I3.read();
			yi[index+3] <= signal_I4.read();

			yq[index] <= signal_Q1.read();
			yq[index+1] <= signal_Q2.read();
			yq[index+2] <= signal_Q3.read();
			yq[index+3] <= signal_Q4.read();

			count <= count + 1;
			index <= index + 4;
			acqflag <= 0;
		endrule

		rule computeData(index == Size && flag == 0);
			c1.readInputs(code,yi,0,sum);
			flag <= 1;
		endrule

		rule getResults(flag == 1);
			let lv_sum = c1.returnSum();
			sum <= lv_sum;
			index <= 0;
		endrule



		rule writeSum(count >= 3000);
			Int#(SumSize) lv_sum = (signExtend(sum)*signExtend(sum))/(signExtend(limit)*signExtend(limit));
			sums.put(True,loopcount,lv_sum);
			index <= 0;
			writeFlag <= 0;
		endrule


		method Bit#(1) returnFlag() if(count >= 3000);
			return 1;
		endmethod
		/*while(prn < prn_max)
		begin
			prn = prn + 1;

			Int#(32) numberOfFrqBins = fromReal(round(acqSearchBand * 1000/step_freq)) + 1;
			for(Int#(32) frqBinIndex = 1; frqBinIndex < numberOfFrqBins; frqBinIndex = frqBinIndex+1)
			begin
				Int#(32) frqBin = iF - (acqSearchBand/2)*1000 + step_freq * (frqBinIndex - 1);
				//let x = mkNco(frqBin,1,samplingFreq/downByFactor,limit,0); 

				Int#(32) count_I = 0;
				Int#(32) count_Q = 0;


					
			end
		end*/
	endmodule // mkAcq

endpackage: acquireBank