package corrBank;
import initial_Settings::*;
import Vector::*;
import FixedPoint::*;
import correlator::*;
import Real::*;
//import code_Generator::*;
import BRAMCore::*;
	
//`define Size 100
	typedef 100 BankSize;

	interface CorrBankIfc;
		method Action readInputs(Vector#(Size,Bit#(2)) code, Vector#(Size,Int#(BitSize)) yi,Int#(BitSize) count1, Int#(SumSize) sum1);
		method Int#(SumSize) returnSum();
	endinterface : CorrBankIfc
	//Vector#(12000, Int#(BitSize)) code, I1, Q1;

	module mkBank(CorrBankIfc);
		Integer bsize = valueOf(BankSize);

		Reg#(Bit#(2)) readFlag <- mkReg(0);
		//Corr_ifc correlator_inst[BankSize];
		//corr_ifc#() correlator_inst_Q [Size];
		Vector#(BankSize,Corr_ifc) correlator_inst;

		Integer j = 0;
		for(j=0; j<bsize; j=j+1)
		begin
			correlator_inst [j] <- mkCorr();
		end

		Vector#(Size, Reg#(Bit#(2))) code <- replicateM(mkReg(0));
		Vector#(Size, Reg#(Int#(BitSize))) yi <- replicateM(mkReg(0));

		Reg#(Int#(BitSize)) i <- mkReg(0);
		Reg#(Int#(BitSize)) count <- mkReg(0);

		Vector#(BankSize,Reg#(Int#(SumSize))) sum <- replicateM(mkReg(0));
		Vector#(BankSize,Reg#(Bit#(1))) flag <- replicateM(mkReg(0));
		Reg#(Int#(SumSize)) total <- mkReg(0);
		

		for(j = 0; j < bsize; j = j+1)
		begin
			rule r2 (flag[j] == 0 && readFlag == 1);
				//$display("Rule 2");
				correlator_inst[j].readData(unpack(signExtend(code[i])),unpack(signExtend(code[i+1])),unpack(signExtend(code[i+2])),unpack(signExtend(code[i+3])),yi[i],yi[i+1],yi[i+2],yi[i+3]);
				i <= i+4;
				flag[j] <= 1;
			endrule
		end
		for(j = bsize; j < 2*bsize; j = j+1)
		begin
			rule r3 (flag[j-bsize] == 1 && readFlag == 1);
				sum[j-bsize] <= correlator_inst[j-bsize].results();
				//count <= count+1;
				//$display("Sum = %d",sum[j-bsize]);
				//flag[j] <= 2;
				if(j == 2*bsize - 1)
					readFlag <= 2;
			endrule
		end

		rule add(readFlag == 2 && count <= fromInteger(bsize));
			if(count == fromInteger(bsize))
				readFlag <= 3;
			else
			begin
				total <= total+sum[count];
				count <= count+1;
			end
		endrule
		
		method Action readInputs(Vector#(Size,Bit#(2)) code1, Vector#(Size,Int#(BitSize)) y1,Int#(BitSize) count1, Int#(SumSize) sum1);

			for(Int#(BitSize) j = 0; j < fromInteger(valueOf(Size)); j = j+1)
			begin
				code[j] <= code1[j];
				yi[j] <= y1[j];
			end
			total <= sum1;
			count <= count1;
			readFlag <= 1;
		endmethod

		method Int#(SumSize) returnSum () if(readFlag == 3);
			return total;
		endmethod	
	endmodule // mkBank

	function Int#(BitSize) generate1(Integer a);
		Int#(BitSize) x = fromInteger(a);
		return x;
	endfunction : generate1

	module mkTb();
		CorrBankIfc c1 <-mkBank;

		Vector#(Size,Bit#(2)) code = replicate(1);
		Vector#(Size,Int#(BitSize)) y = genWith(generate1);

		Reg#(Int#(SumSize)) ans <- mkReg(0);

		code[3] = 2'b-1;
		code[43] = 2'b-1;
		code[97] = 2'b-1;
		code[54] = 2'b-1;
		code[71] = 2'b-1;

		Reg#(Bit#(4)) flag <- mkReg(0);

		rule tb4 (flag == 0);
			c1.readInputs(code,y,0,ans);
			flag <= 1;
		endrule

		rule tb5 (flag == 1);
			let lv_ans = c1.returnSum();
			ans <= lv_ans;
			$display("lv_ans = %d",lv_ans);

			flag <= 2;
		endrule
		rule tb6(flag == 2);
			$display("ans = %d",ans);
			$finish(0);
		endrule
	endmodule // mkTb
endpackage : corrBank
