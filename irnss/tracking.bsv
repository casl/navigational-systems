package tracking;
	import Vector ::*;
	import code_generator_tracking ::*;
	import processing ::*;
	import initial_Settings ::*;
	import Real ::*;

	interface TrackingIFC;
		method Action sincos();
		method Action Loops();
	endinterface
	
	module tracking(TrackingIFC);
		Vector#(samples_per_code)
		Reg#(Bit#(2)) track <- mkReg(0);
		Reg#(Bit#(1)) track <- mkReg(1);
		Reg#(Bit#(3)) flag <- mkReg(0);


		rule start(start==1);
			status = trackResults[channelNr].acq_Skip[loopcnt];
		    prn_no = trackResults[channelNr].prn;
		    ref_code = code_generator_tracking(trackResults,channelNr,loopcnt);
			start <= 0;
		endrule 

		rule acqStatus(status!=1);
			if (status==0)
			    trackResults = Acquisition(prn_no,ref_code,trackResults,rawdata,loopcnt,channelNr,status);
			else if (status==2)
			    trackResults = Acquisition(prn_no,ref_code,trackResults,rawdata,loopcnt,channelNr,status);
			    track <= 1;
		endrule
		
		rule tracking(status==1 || track==1)
		    samples_per_code = length(ref_code); 
		    rawSignal = rawdata([loopcnt-1]*samples_per_code+1:loopcnt*samples_per_code);		        
		    frqBin = trackResults[channelNr].carrFreq[loopcnt-1];
		    Phase_carr = trackResults[channelNr].phi[loopcnt-1];
		            
		    [carrSin,carrCos,lastPhase] = nco(frqBin,loopcnt,samplingFreq,samples_per_code,Phase_carr);


		    c1.readInputs(carrSin,rawSignal,0,qBasebandSignal); // Quaderature
		    c1.readInputs(carrCos,rawSignal,0,iBasebandSignal); // In-Phase


			samplesPerCodeChip = round(settings.samplingFreq / settings.codeFreqBasis);
			promptDelay  = trackResults[channelNr].codephase[loopcnt-1]; 
			earlyDelay   = promptDelay-samplesPerCodeChip/2; 
			lateDelay    = promptDelay+samplesPerCodeChip/2; 
			//--------------------
			//rule r3
			promptCode   = [ref_code(end-promptDelay+1:end) ref_code(1:end-promptDelay)];
			earlyCode    = [ref_code(end-earlyDelay+1:end) ref_code(1:end-earlyDelay)];
			lateCode     = [ref_code(end-lateDelay+1:end) ref_code(1:end-lateDelay)];
			flag <= 1;
		endrule
		           
		// Calculate six standard Correlation Values (Code wipe-off and accumulation)
		
		rule computerefs(flag==1);
			c1.readInputs(earlyCode,iBasebandSignal,0,i_E);
			c1.readInputs(earlyCode,qBasebandSignal,0,q_E);
			c1.readInputs(promptCode,iBasebandSignal,0,i_P);
			c1.readInputs(promptCode,qBasebandSignal,0,q_p);
			c1.readInputs(lateCode,iBasebandSignal,0,i_L);
			c1.readInputs(lateCode,qBasebandSignal,0,q_L);
			flag <= 2;
		endrule

		rule updateresults(flag <= 2);	          
		// Fill in the correlation values in results
			trackResults[channelNr].I_E[loopcnt] = I_E;
			trackResults[channelNr].Q_E[loopcnt] = Q_E;
			trackResults[channelNr].I_P[loopcnt] = I_P;
			trackResults[channelNr].Q_P[loopcnt] = Q_P;
			trackResults[channelNr].I_L[loopcnt] = I_L;
			trackResults[channelNr].Q_L[loopcnt] = Q_L;
		endrule 

		     //// ================== Tracking Loops ========================== 
		// There are two loops : Code tracking and Carrier tracking
		// Carrier tracking consisits of PLL and FLL
		  
		//// Carrier Tracking Loop

		// Phase Discriminator :
		method Action Loops(Empty);
			theta = fromReal(atan((trackResults[channelNr].I_P[loopcnt])/(trackResults[channelNr].Q_P[loopcnt])));
			trackResults[channelNr].theta[loopcnt] = theta;

			// Frequency Discriminator :

			// As two values of correlator outputs or phase discriminator outputs are
			// required FLL loop update time is twice as that of PLL
			// As FLL is used Phase values are also updated with FLL update rate
			if ((loopcnt%2) == 1) 
			begin
			    frqDisc = (trackResults[channelNr].theta[loopcnt]-trackResults[channelNr].theta[loopcnt-1])/(2*pi*1e-3);
			    trackResults[channelNr].freqdev[loopcnt]=frqDisc;

			    // Frequency Loop Filter :

			    trackResults[channelNr].filtfreqdev[loopcnt]=settings.k2_freq*trackResults[channelNr].filtfreqdev[loopcnt-1]+...
			        settings.k1_freq*frqDisc;

			    // Phase Loop Filter :

			    trackResults[channelNr].filttheta[loopcnt]=settings.k2_phase*trackResults[channelNr].filttheta[loopcnt-2]+...
			        settings.k1_phase*theta;
			    // Update NCO phase and frequency for next loop : 
			    trackResults[channelNr].phi[loopcnt] = lastPhase+trackResults[channelNr].filttheta[loopcnt];//accumulated phase for NCO
			    trackResults[channelNr].carrFreq[loopcnt] = frqBin+trackResults[channelNr].filtfreqdev[loopcnt];//Frequency for NCO
			end
			else // No updates at 1 ms (one code period)
			begin
			    trackResults[channelNr].freqdev[loopcnt]=trackResults[channelNr].freqdev[loopcnt-1];
			    trackResults[channelNr].filtfreqdev[loopcnt]=trackResults[channelNr].filtfreqdev[loopcnt-1];
			    trackResults[channelNr].phi[loopcnt] = lastPhase;
			    trackResults[channelNr].carrFreq[loopcnt] = frqBin;    
			end

			// Measure overall Doppler at every loopcnt
			trackResults[channelNr].DopplerFreq[loopcnt]=trackResults[channelNr].carrFreq[loopcnt]-settings.IF;
			//// Code Tracking Loop

			// Calculate the Envelope values
			E = fromReal(sqrt(I_E^2+Q_E^2)); // Early  Envelope
			P = fromReal(sqrt(I_P^2+Q_P^2)); // Prompt Envelope
			L = fromReal(sqrt(I_L^2+Q_L^2)); // Late   Envelope
			////  E-L (Code Discriminator):
			codeError = (E - L)/(E + L);
			trackResults[channelNr].codeError[loopcnt] = codeError; 
			// Check E-L and update code phase accordingly

			// check if E-L curve has a zero crossing or not
			if (trackResults[channelNr].codeError[loopcnt]*trackResults[channelNr].codeError[loopcnt-1]) >= 0 // No zero crossing
			begin
			    if trackResults[channelNr].codeError[loopcnt] > 0 // Positive, shift in left
			         trackResults[channelNr].codephase[loopcnt] = promptDelay-1;
			    elseif trackResults[channelNr].codeError[loopcnt] < 0 // Negative, shift in right  
			         trackResults[channelNr].codephase[loopcnt] = promptDelay+1;
			    else // E-L=0, No shift required
			         trackResults[channelNr].codephase[loopcnt] = promptDelay;
			end    
			else // zero crossing in E-L, no need to shift
			   trackResults[channelNr].codephase[loopcnt] = promptDelay;
			end


			//// Code Phase Filter (Histogram)

			if (loopcnt%20)==0
			begin
			            hist_array=trackResults[channelNr].codephase(loopcnt-19:loopcnt); //last 20 values
			            min_shift=min(hist_array); // determine max 
			            max_shift=max(hist_array); // min value of code phase
			            hist_length=max_shift-min_shift+1; // determine total enteries in histogram
			            hist_indices = min_shift:1:max_shift; // generate indices
			            hist_freq=zeros(1,hist_length); // initialize histogram
			            // calculate frequency of each code phase using loops
			            for i=1:20
			                for j=1:hist_length
			                    if hist_indices(j)==hist_array(i)
			                        hist_freq(j)=hist_freq(j)+1;
			                    end
			                end
			            end    
			            [~, hist_index]=max(hist_freq); // determine maximum frequency
			            trackResults[channelNr].filtcodephase[loopcnt]=hist_indices(hist_index); // update result
			end
			else
				trackResults[channelNr].filtcodephase[loopcnt]=trackResults[channelNr].filtcodephase[loopcnt-1]; //pass on previous
		endmethod
	endmodule
endpackage