package remainder;

	interface Remainder_Ifc;
		method Action remInput(Int#(64) n, Int#(64) d);
		method Int#(64) returnData();
		method Bit#(1) returnFlag();
	endinterface
	module mkRem(Remainder_Ifc);
		Reg#(Bit#(2)) flag <- mkReg(0);
		Reg#(Bit#(1)) endflag <- mkReg(0);

		Reg#(Int#(64)) count <- mkReg(0);
		Reg#(Int#(64)) rem <- mkReg(0);

		Reg#(Int#(64)) num <- mkReg(0);
		Reg#(Int#(64)) den <- mkReg(0);


		rule counter(flag == 1 && endflag == 0);
			$display("Count : %d, %d",rem,count);
			if(rem == den)
			begin
				$display("Resetting...");
				flag <= 2;
			end
			else if(count >= num)
			begin
				endflag <= 1;
			end
			else
			begin
				rem <= rem+1;
				count <= count+1;
			end
		endrule
		
		rule reset(flag == 2 && endflag == 0);
			$display("Reset : %d, %d",rem,count);
			flag <= 1;
			rem <= 0;
		endrule

		method Action remInput(Int#(64) n,Int#(64) d) if(flag == 0 && endflag == 0);
			num <= n;
			den <= d;
			flag <= 1;
			$display("Inputs read");
		endmethod

		method Int#(64) returnData() if(endflag == 1);
			return rem;
		endmethod

		method Bit#(1) returnFlag() if(endflag == 1);
			return 1;
		endmethod

	endmodule // mkRem
	module mkTb();
		Reg#(Int#(64)) ans <- mkReg(0);
		Remainder_Ifc rem1 <- mkRem;
		Reg#(Bit#(2)) flag <- mkReg(0);
		rule r1 (flag == 0);
			//$display("Rule R1");
			rem1.remInput(24552,12000);
			flag <= 1;
		endrule
		rule r2(flag == 1);
			//$display("Rule R2");
			let lv = rem1.returnData();
			ans <= lv;
			flag <= 2;
		endrule
		rule r3(flag == 2);
			$display("Remainder = %d",ans);
			$finish(0);
		endrule
	endmodule // mkTb

endpackage : remainder