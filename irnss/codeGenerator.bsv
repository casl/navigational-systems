package codeGenerator;
	import initial_Settings::*;
	import Vector::*;
	//import generatePRN::*;
	import prn1::*;
	import BRAMCore::*;

	interface CodeGeneratorIfc;
		method Action r0();
		method Action r1(Int#(16) i);
		method Action r2(Int#(16) i);
		method Action readData(Int#(8) prn1, Int#(8) downByFactor1);
		//method Bit#(1) returnCode();
		method Action readReqBRAM(Int#(16) j);
		method Bit#(2) readFromBRAM();
	endinterface

	module mkCode(CodeGeneratorIfc);
		Reg#(Int#(8)) prn <- mkReg(0);
		Reg#(Int#(8)) downByFactor <- mkReg(0);
		Reg#(Bit#(2)) flag <- mkReg(0);

		//Reg#(Int#(16)) i <- mkReg(0);
		Reg#(Int#(25)) temp <- mkReg(0);

		Vector#(1023,Reg#(Bit#(2))) prnCode <- replicateM(mkReg(1));

		//Vector#(12000,Reg#(Bit#(2))) code <- replicateM(mkReg(0));
		BRAM_PORT#(Int#(16),Bit#(2)) code <- mkBRAMCore1(12000,False);

		method Action r0();
			prnCode[5] <= -1;
			prnCode[64] <= -1;
			prnCode[133] <= -1;
			prnCode[197] <= -1;
			prnCode[240] <= -1;
			prnCode[311] <= -1;
			prnCode[367] <= -1;
			prnCode[499] <= -1;	
			//$display("Rule r1");
			flag <= 1;
		endmethod

		method Action r1(Int#(16) i);
			Int#(25) x = ((1023*(signExtend(i)+1))/12000);
			//let y = (x%1023);
			temp <= x;
			//$display("Rule r2");
			//$display("%d",y);
			flag <= 2;
		endmethod

		method Action r2(Int#(16) i);
			//code[i] <= prnCode[temp];
			$display("Temp=%d",temp);
			code.put(True,i,prnCode[temp]);
			//$display("Rule r3");
			//i <= i+1;
			flag <= 1;
		endmethod


		method Action readData(Int#(8) prn1, Int#(8) downByFactor1);
			prn <= prn1;
			downByFactor <= downByFactor1;

			flag <= -1;
		endmethod

		method Action readReqBRAM(Int#(16) j);
			code.put(False,j,?);
		endmethod
		method Bit#(2) readFromBRAM();
			Bit#(2) x = 0;
			x = code.read();
			return x;
		endmethod

	endmodule // mkCode

	module mkTb();
		CodeGeneratorIfc ifc <- mkCode;

		Reg#(Bit#(2)) flag <- mkReg(0);
		Reg#(Int#(16)) count <- mkReg(0);
		Reg#(Bit#(1)) k1 <- mkReg(0);
		Reg#(Bit#(1)) acqFlag<- mkReg(0);

		Reg#(Int#(16)) readCount <- mkReg(0);


		rule x0(flag == 0);
			//$display("Rule x1");
			ifc.readData(1,1);
			flag <= 1;
		endrule
		rule x1(flag == 1);
			ifc.r0();
			//$display("Rule x2");
			flag <=2;
		endrule
		rule x2(flag == 2 && k1 == 0 && count<12000);
			ifc.r1(count);
			k1<=1;
		endrule
		rule x3(flag == 2 && k1 == 1 && count<12000);
			ifc.r2(count);
			count <= count +1;
			k1<=0;
		endrule
		rule bram1(count >= 12000 && acqFlag == 0 && readCount < 200);
			ifc.readReqBRAM(readCount);
			acqFlag <= 1;
			readCount <= readCount+10;
		endrule
		rule x4(count >= 12000 && acqFlag == 1 && readCount < 200);
			let lv = ifc.readFromBRAM();
			$display("%d, %b",readCount,lv);
			acqFlag <= 0;
		endrule
		rule x5(readCount >= 200);
			$display("%b",11999);
			$display("Finish");
			$finish(0);
		endrule
	endmodule 
endpackage