package prn1;
	import initial_Settings::*;
	import Vector::*;

	function Vector#(1023,Bit#(2)) getPRN(Int#(8) prn);

		Vector#(1023,Bit#(2)) prnCode = replicate(1);
		prnCode[5] = -1;
		prnCode[64] = -1;
		prnCode[133] = -1;
		prnCode[197] = -1;
		prnCode[240] = -1;
		prnCode[311] = -1;
		prnCode[367] = -1;
		prnCode[499] = -1;	

		return prnCode;
	endfunction
endpackage